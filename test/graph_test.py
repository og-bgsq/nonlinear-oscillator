from unittest import TestCase
from pyno.graph.graph import *
from pyno.graph.graph_generator import *
import random
from .test_tools import *

class GraphTest(TestCase):
    def test_calc_dx_lin(self):
        a = random.random() * 1000
        g = Graph("", 0, 0.1, [a * i * 0.1 for i in range(10000)])
        g.calc_dx()

        self.assertTrue(is_list_equal_variance([a] * 10000, g.dx))

    def test_calc_dx_sin(self):
        a = random.random()
        g = Graph("", 0, 0.01, [math.sin(a * i * 0.01) for i in range(10000)])
        g.calc_dx()

        dx = [a * math.cos(a * i * 0.01) for i in range(10000)]

        self.assertTrue(is_list_equal_variance(dx, g.dx))

    def test_calc_dx_exp(self):
        a = random.random() / 1000
        g = Graph("", 0, 0.1, [math.exp(a * i * 0.1) for i in range(10000)])
        g.calc_dx()

        dx = [a * math.exp(a * i * 0.1) for i in range(10000)]

        self.assertTrue(is_list_equal_variance(dx, g.dx))

class CreateFunctionTest(TestCase):
    def test_create_function_linear(self):
        a = random.random() * 1000
        g = [a * i for i in range(10000)]
        f = create_function(str(a) + "*x")

        self.assertTrue(is_list_equal_variance(g, [f(x) for x in range(10000)]))

    def test_create_function_math(self):
        a = random.random()
        b = random.random()
        c = random.random() / 1000
        g = [math.sin(a*i)+math.cos(b*i*math.pi)+math.exp(c*i) for i in range(10000)]
        f = create_function("sin(" + str(a) + "*x)+cos(" + str(b) + "*x*pi)+exp(" + str(c) + "*x)")

        self.assertTrue(is_list_equal_variance(g, [f(x) for x in range(10000)]))

    def test_create_fa_function(self):
        amp = random.random() * 1000
        freq = random.random() * 1000
        g = [amp * math.sin(i * freq) for i in range(10000)]
        f = create_fa_function(str(amp) + "*sin(t*" + str(freq) + ")")

        self.assertTrue(is_list_equal_variance(g, [f(freq, amp, t) for t in range(10000)]))

    def test_invalid_function(self):
        try:
            create_function("jdwuial")
            self.assertTrue(False)
        except:
            self.assertTrue(True)

    def test_invalid_fa_function(self):
        try:
            create_fa_function("a*sin(f*x)")
            self.assertTrue(False)
        except:
            self.assertTrue(True)

class GraphFromEquationTest(TestCase):
    def test_generate_list(self):
        t0 = 0
        dt = 0.001
        time = [t0 + i * dt for i in range(10000)]
        
        def f(t):
            return 10*math.sin(0.1*t)
        
        x1 = [f(t) for t in time]
        x2 = generate_list(t0, dt, 10000, f)

        self.assertTrue(is_list_equal_variance(x1, x2))

    def test_graph(self):
        t0 = 0
        dt = 0.001
        time = [t0 + i * dt for i in range(10000)]
        x = [math.sin(2*t)**2+3*math.cos(-t) for t in time]
        dx = [4*math.cos(2*t)*math.sin(2*t)-3*math.sin(t) for t in time]
        g = graph_from_equation("sin(2*x)**2+3*cos(-x)", t0, dt, 10000)

        self.assertTrue(is_list_equal_variance(g.x, x))
        self.assertTrue(is_list_equal_variance(g.dx, dx))
        self.assertEqual(g.dt, dt)
        self.assertEqual(g.t0, t0)
        self.assertEqual(g.name, "sin(2*x)**2+3*cos(-x)")