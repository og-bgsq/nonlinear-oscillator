from unittest import TestCase
from pyno.solver import *
import pyno.solver.solve_ode as sol
from pyno.files import load_data_sets
from defines import ROOT_DIR
from .test_tools import *
import math
import cmath
import os

class TestSolvers(TestCase):
    def test_solve_damped_harmonic(self):
        sol.solver = "lsoda"
        dh1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_damped_harmonic_test.bin"))
        dh2 = solve_damped_harmonic(0, 0.01, 10000, 0, 1, 1, 10, 0.01)

        self.assertTrue(is_list_equal_variance(dh1.x, dh2[0]))
        self.assertTrue(is_list_equal_variance(dh1.dx, dh2[1]))
        
    def test_solve_forced_damped_harmonic(self):
        sol.solver = "lsoda"
        dfh1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_forced_damped_harmonic_test.bin"))
        dfh2 = solve_forced_damped_harmonic(0, 0.01, 10000, 0, 1, 1, 10, 0.01, lambda t : math.sin(t))

        self.assertTrue(is_list_equal_variance(dfh1.x, dfh2[0]))
        self.assertTrue(is_list_equal_variance(dfh1.dx, dfh2[1]))
        
    def test_solve_van_der_pol(self):
        sol.solver = "lsoda"
        vdp1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_van_der_pol_test.bin"))
        vdp2 = solve_van_der_pol(0, 0.01, 10000, 0, 1, 2)

        self.assertTrue(is_list_equal_variance(vdp1.x, vdp2[0]))
        self.assertTrue(is_list_equal_variance(vdp1.dx, vdp2[1]))
        
    def test_solve_forced_van_der_pol(self):
        sol.solver = "lsoda"
        fvdp1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_forced_van_der_pol_test.bin"))
        fvdp2 = solve_forced_van_der_pol(0, 0.01, 10000, 0, 1, 2, lambda t : 3 * math.sin(t * 1.1))

        self.assertTrue(is_list_equal_variance(fvdp1.x, fvdp2[0]))
        self.assertTrue(is_list_equal_variance(fvdp1.dx, fvdp2[1]))
        
    def test_solve_vilfan_duke(self):
        sol.solver = "vode"
        vd1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_vilfan_duke_test.bin"))
        vd2 = solve_vilfan_duke(0, 0.01, 10000, 1j, 1, 10, 0.01, 3, 0.1, lambda t : math.sin(t), 1.5, lambda t : math.sin(t*3), lambda t : math.sin(t*0.3))

        self.assertTrue(is_list_equal_variance(vd1.x, vd2[0]))
        self.assertTrue(is_list_equal_variance(vd1.dx, vd2[1]))
        
    def test_solve_vilfan_duke_chain(self):
        sol.solver = "vode"
        vdc1 = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_vilfan_duke_chain_test.bin"))
        vdc2 = solve_vilfan_duke_chain(0, 0.01, 10000, 10, [cmath.exp(1j*2*math.pi*x/10) for x in range(10)], [x+1 for x in range(10)], [10-x for x in range(10)], [x * 0.01 for x in range(10)], [3 for _ in range(10)], [0.01 for _ in range(10)], [lambda t : 0.3*math.sin(t)]*10, [x*0.2 for x in range(10)])

        for (g1, g2) in zip(vdc1, vdc2):
            self.assertTrue(is_list_equal_variance(g1.x, [s[0].real for s in g2]))
            self.assertTrue(is_list_equal_variance(g1.dx, [s[0].imag for s in g2]))
        
    def test_solve_van_der_pol_coupled(self):
        sol.solver = "lsoda"
        vdpc1 = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_van_der_pol_coupled_test.bin"))
        vdpc2 = solve_van_der_pol_coupled(0, 0.01, 10000, [math.cos(x * 2 * math.pi * x / 10) for x in range(10)], [math.sin(x * 2 * math.pi * x / 10) for x in range(10)], [x*0.5 for x in range(10)], [1 for _ in range(10)], 10)

        itr = iter(vdpc2)

        for (g1, g2, gd2) in zip(vdpc1, itr, itr):
            self.assertTrue(is_list_equal_variance(g1.x, g2))
            self.assertTrue(is_list_equal_variance(g1.dx, gd2))

class TestRealTimeSolvers(TestCase):
    def test_solve_damped_harmonic_rt(self):
        sol.solver = "vode"
        dh1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_damped_harmonic_test.bin"))
        dhr = get_damped_harmonic_rt(0, 0, 1, 1, 10, 0.01)
        for y1, dy1 in zip(dh1.x[1:], dh1.dx[1:]):
            y2, dy2 = dhr(0.01)
            self.assertTrue(is_equal_variance(y1, y2) and is_equal_variance(dy1, dy2))
        
    def test_solve_forced_damped_harmonic_rt(self):
        sol.solver = "vode"
        dfh1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_forced_damped_harmonic_test.bin"))
        dfhr = get_forced_damped_harmonic_rt(0, 0, 1, 1, 10, 0.01, lambda t : math.sin(t))
        for y1, dy1 in zip(dfh1.x[1:], dfh1.dx[1:]):
            y2, dy2 = dfhr(0.01)
            self.assertTrue(is_equal_variance(y1, y2) and is_equal_variance(dy1, dy2))
        
    def test_solve_van_der_pol_rt(self):
        sol.solver = "vode"
        vdp1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_van_der_pol_test.bin"))
        vdpr = get_van_der_pol_rt(0, 0, 1, 2)
        for y1, dy1 in zip(vdp1.x[1:], vdp1.dx[1:]):
            y2, dy2 = vdpr(0.01)
            self.assertTrue(is_equal_variance(y1, y2) and is_equal_variance(dy1, dy2))
        
    def test_solve_forced_van_der_pol_rt(self):
        sol.solver = "vode"
        fvdp1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_forced_van_der_pol_test.bin"))
        fvdpr = get_forced_van_der_pol_rt(0, 0, 1, 2, lambda t : 3 * math.sin(t * 1.1))
        for y1, dy1 in zip(fvdp1.x[1:], fvdp1.dx[1:]):
            y2, dy2 = fvdpr(0.01)
            self.assertTrue(is_equal_variance(y1, y2) and is_equal_variance(dy1, dy2))
        
    def test_solve_vilfan_duke_rt(self):
        sol.solver = "vode"
        vd1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "solve_vilfan_duke_test.bin"))
        vdr = get_vilfan_duke_rt(0, 1j, 1, 10, 0.01, 3, 0.1, lambda t : math.sin(t), 1.5, lambda t : math.sin(t*3), lambda t : math.sin(t*0.3))
        for y1, dy1 in zip(vd1.x[1:], vd1.dx[1:]):
            y2, dy2 = vdr(0.01)
            self.assertTrue(is_equal_variance(y1, y2) and is_equal_variance(dy1, dy2))