from unittest import TestCase
import math
import random

from pyno.entrainment.entrainment import *
from pyno.entrainment.entrainment import _get_samples_per_revolutions, _get_winding_number, _get_angle_from_amp_vel, _has_N_period, _angle_diff, _min_angle_diff
from .test_tools import *

s = [math.sin(i * 0.01 * 2 * math.pi) for i in range(10000)]
s2 = [math.sin(i * 0.01 * 2 * math.pi + 0.1) for i in range(10000)]
s3 = [math.cos(i * 0.01 * 2 * math.pi) for i in range(10000)]
s4 = [math.sin(0.9 * i * 0.01 * 2 * math.pi) for i in range(10000)]
sr = [math.sin(-i * 0.01 * 2 * math.pi) for i in range(10000)]
sr2 = [math.sin(-i * 0.01 * 2 * math.pi + 0.1) for i in range(10000)]
sr3 = [math.cos(-i * 0.01 * 2 * math.pi) for i in range(10000)]
sr4 = [math.sin(-0.9 * i * 0.01 * 2 * math.pi) for i in range(10000)]

sv = [2 * math.pi * math.cos(i * 0.01 * 2 * math.pi) for i in range(10000)]
s2v = [2 * math.pi * math.cos(i * 0.01 * 2 * math.pi + 0.1) for i in range(10000)]
s3v = [2 * math.pi * -math.sin(i * 0.01 * 2 * math.pi) for i in range(10000)]
s4v = [0.9 * 2 * math.pi * math.cos(0.9 * i * 0.01 * 2 * math.pi) for i in range(10000)]
srv = [2 * math.pi * math.cos(i * 0.01 * 2 * math.pi) for i in range(10000)]
sr2v = [2 * math.pi * math.cos(i * 0.01 * 2 * math.pi + 0.1) for i in range(10000)]
sr3v = [-2 * math.pi * -math.sin(i * 0.01 * 2 * math.pi) for i in range(10000)]
sr4v = [0.9 * 2 * math.pi * math.cos(0.9 * i * 0.01 * 2 * math.pi) for i in range(10000)]

class TestHilbertEntrained(TestCase):
    def test_hilbert_entrained_self_cw(self):
        self.assertTrue(is_entrained_hilbert(s, s))
    
    def test_hilbert_entrained_phase_shift_cw(self):
        self.assertTrue(is_entrained_hilbert(s, s2))
    
    def test_hilbert_entrained_cos_cw(self):
        self.assertTrue(is_entrained_hilbert(s, s3))
    
    def test_hilbert_entrained_lower_freq_cw(self):
        self.assertFalse(is_entrained_hilbert(s, s4))
        
    def test_hilbert_entrained_neg_cw(self):
        self.assertTrue(is_entrained_hilbert(s, sr))
        

    def test_hilbert_entrained_self_ccw(self):
        self.assertTrue(is_entrained_hilbert(sr, sr))
        
    def test_hilbert_entrained_phase_ccw(self):
        self.assertTrue(is_entrained_hilbert(sr, sr2))
        
    def test_hilbert_entrained_cos_ccw(self):
        self.assertTrue(is_entrained_hilbert(sr, sr3))
        
    def test_hilbert_entrained_lower_freq_ccw(self):
        self.assertFalse(is_entrained_hilbert(sr, sr4))
        
    def test_hilbert_entrained_neg_ccw(self):
        self.assertTrue(is_entrained_hilbert(sr, s))

class TestWindingEntrained(TestCase):
    def test_winding_entrained_self_cw(self):
        self.assertTrue(is_entrained_winding_numbers(list(zip(s, sv)), list(zip(s, sv))))

    def test_winding_entrained_phase_cw(self):
        self.assertTrue(is_entrained_winding_numbers(list(zip(s, sv)), list(zip(s2, s2v))))
        
    def test_winding_entrained_cos_cw(self):
        self.assertTrue(is_entrained_winding_numbers(list(zip(s, sv)), list(zip(s3, s3v))))
        
    def test_winding_entrained_lower_freq_cw(self):
        self.assertFalse(is_entrained_winding_numbers(list(zip(s, sv)), list(zip(s4, s4v))))
        
    def test_winding_entrained_neg_cw(self):
        self.assertFalse(is_entrained_winding_numbers(list(zip(s, sv)), list(zip(sr, srv))))

        
    def test_winding_entrained_self_ccw(self):
        self.assertTrue(is_entrained_winding_numbers(list(zip(sr, srv)), list(zip(sr, srv))))
        
    def test_winding_entrained_phase_ccw(self):
        self.assertTrue(is_entrained_winding_numbers(list(zip(sr, srv)), list(zip(sr2, sr2v))))
        
    def test_winding_entrained_cos_ccw(self):
        self.assertTrue(is_entrained_winding_numbers(list(zip(sr, srv)), list(zip(sr3, sr3v))))
        
    def test_winding_entrained_lower_freq_ccw(self):
        self.assertFalse(is_entrained_winding_numbers(list(zip(sr, srv)), list(zip(sr4, sr4v))))
        
    def test_winding_entrained_neg_ccw(self):
        self.assertFalse(is_entrained_winding_numbers(list(zip(sr, srv)), list(zip(s, sv))))

class TestWindingNumbers(TestCase):
    def test_winding_num_self_cw(self):
        self.assertAlmostEqual(get_winding_numbers(list(zip(s, sv)), list(zip(s, sv))), 1, 2)

    def test_winding_num_phase_cw(self):
        self.assertAlmostEqual(get_winding_numbers(list(zip(s, sv)), list(zip(s2, s2v))), 1, 2)

    def test_winding_num_cos_cw(self):
        self.assertAlmostEqual(get_winding_numbers(list(zip(s, sv)), list(zip(s3, s3v))), 1, 2)
        
    def test_winding_num_lower_freq_cw(self):
        self.assertAlmostEqual(get_winding_numbers(list(zip(s, sv)), list(zip(s4, s4v))), 1/0.9, 2)
        self.assertNotAlmostEqual(get_winding_numbers(list(zip(s, sv)), list(zip(s4, s4v))), 1, 2)
        

    def test_winding_num_self_ccw(self):
        self.assertAlmostEqual(get_winding_numbers(list(zip(sr, sv)), list(zip(sr, srv))), 1, 2)
        
    def test_winding_num_phase_ccw(self):
        self.assertAlmostEqual(get_winding_numbers(list(zip(sr, sv)), list(zip(sr2, sr2v))), 1, 2)
        
    def test_winding_num_cos_ccw(self):
        self.assertAlmostEqual(get_winding_numbers(list(zip(sr, sv)), list(zip(sr3, sr3v))), 1, 2)
        
    def test_winding_num_lower_ccw(self):
        self.assertAlmostEqual(get_winding_numbers(list(zip(sr, sv)), list(zip(sr4, sr4v))), 1/0.9, 2)
        self.assertNotAlmostEqual(get_winding_numbers(list(zip(sr, sv)), list(zip(sr4, sr4v))), 1, 2)
        
class TestWindingNumbersRev(TestCase):
    def test_winding_num_rev_cw(self):
        self.assertAlmostEqual(get_winding_number_rev(list(zip(s, sv)), 100), 1, 2)

    def test_winding_num_rev_phase_cw(self):
        self.assertAlmostEqual(get_winding_number_rev(list(zip(s2, s2v)), 100), 1, 2)
        
    def test_winding_num_rev_cos_cw(self):
        self.assertAlmostEqual(get_winding_number_rev(list(zip(s3, s3v)), 100), 1, 2)
        
    def test_winding_num_rev_lower_freq_cw(self):
        self.assertAlmostEqual(get_winding_number_rev(list(zip(s4, s4v)), 100), 0.9, 2)
        self.assertNotAlmostEqual(get_winding_number_rev(list(zip(s4, s4v)), 100), 1, 2)
        

    def test_winding_num_rev_ccw(self):
        self.assertAlmostEqual(get_winding_number_rev(list(zip(sr, srv)), 100), 1, 2)
        
    def test_winding_num_rev_phase_ccw(self):
        self.assertAlmostEqual(get_winding_number_rev(list(zip(sr2, sr2v)), 100), 1, 2)
        
    def test_winding_num_rev_cos_ccw(self):
        self.assertAlmostEqual(get_winding_number_rev(list(zip(sr3, sr3v)), 100), 1, 2)
        
    def test_winding_num_rev_lower_freq_ccw(self):
        self.assertAlmostEqual(get_winding_number_rev(list(zip(sr4, sr4v)), 100), 0.9, 2)
        self.assertNotAlmostEqual(get_winding_number_rev(list(zip(sr4, sr4v)), 100), 1, 2)
        
class TestWindingNumbersSpr(TestCase):
    def test_winding_num_spr_cw(self):
        self.assertAlmostEqual(get_winding_number_spr(list(zip(s, sv)), 90, 0.01, 2 * math.pi), 1, 2)

    def test_winding_num_spr_phase_cw(self):
        self.assertAlmostEqual(get_winding_number_spr(list(zip(s2, s2v)), 90, 0.01, 2 * math.pi), 1, 1)
        
    def test_winding_num_spr_cos_cw(self):
        self.assertAlmostEqual(get_winding_number_spr(list(zip(s3, s3v)), 90, 0.01, 2 * math.pi), 1, 2)
        
    def test_winding_num_spr_lower_freq_cw(self):
        self.assertAlmostEqual(get_winding_number_spr(list(zip(s4, s4v)), 50, 0.01, 0.9 * 2 * math.pi), 1, 2)
        self.assertNotAlmostEqual(get_winding_number_spr(list(zip(s4, s4v)), 50, 0.01, 2 * math.pi), 1, 2)
        
        
    def test_winding_num_spr_ccw(self):
        self.assertAlmostEqual(get_winding_number_spr(list(zip(sr, srv)), 90, 0.01, 2 * math.pi), 1, 2)
        
    def test_winding_num_spr_phase_ccw(self):
        self.assertAlmostEqual(get_winding_number_spr(list(zip(sr2, sr2v)), 90, 0.01, 2 * math.pi), 1, 1)
        
    def test_winding_num_spr_cos_ccw(self):
        self.assertAlmostEqual(get_winding_number_spr(list(zip(sr3, sr3v)), 90, 0.01, 2 * math.pi), 1, 2)
        
    def test_winding_num_spr_lower_freq_ccw(self):
        self.assertAlmostEqual(get_winding_number_spr(list(zip(sr4, sr4v)), 50, 0.01, 0.9 * 2 * math.pi), 1, 2)
        self.assertNotAlmostEqual(get_winding_number_spr(list(zip(sr4, sr4v)), 50, 0.01, 2 * math.pi), 1, 2)
        

class TestLastZeroCrossing(TestCase):
    s1 = [math.sin(i * 0.01 * 2 * math.pi) for i in range(100)]
    s2 = [math.sin(i * 0.01 * 2 * math.pi) for i in range(200)]
    s3 = [math.cos(i * 0.01 * 2 * math.pi) for i in range(100)]
    s4 = [math.cos(i * 0.01 * 2 * math.pi) for i in range(200)]

    def test_last_zero_cross(self):
        self.assertEqual(get_last_zero_crossing(self.s1), 50)

    def test_last_zero_cross_double(self):
        self.assertEqual(get_last_zero_crossing(self.s2), 150)

    def test_last_zero_cross_cos(self):
        self.assertEqual(get_last_zero_crossing(self.s3), 75)

    def test_last_zero_cross_cos_double(self):
        self.assertEqual(get_last_zero_crossing(self.s4), 175)


class TestLastPeak(TestCase):
    s1 = [math.sin(i * 0.01 * 2 * math.pi) for i in range(100)]
    s2 = [math.sin(i * 0.01 * 2 * math.pi) for i in range(200)]
    s3 = [math.cos(i * 0.01 * 2 * math.pi) for i in range(101)]
    s4 = [math.cos(i * 0.01 * 2 * math.pi) for i in range(201)]

    def test_last_peak(self):
        self.assertEqual(get_last_peak(self.s1), 75)

    def test_last_peak_double(self):
        self.assertEqual(get_last_peak(self.s2), 175)

    def test_last_peak_cos(self):
        self.assertEqual(get_last_peak(self.s3), 50)

    def test_last_peak_cos_double(self):
        self.assertEqual(get_last_peak(self.s4), 150)


class TestEntrainmentTimes(TestCase):
    s1 = [(math.sin(i * 0.01 * 2 * math.pi * 10), 2 * math.pi * 10 * math.cos(i * 0.01 * 2 * math.pi * 10)) for i in range(1000)]
    s2 = [(math.sin(i * 0.01 * 2 * math.pi), 2 * math.pi * math.cos(i * 0.01 * 2 * math.pi)) for i in range(1000)]

    def test_entrainment_times(self):
        self.s1[100:201] = self.s2[100:201]
        l = [False] * 1000
        l[100] = True

        entrained = get_entrainment_times_wn(self.s1, self.s2)
        self.assertTrue(entrained[100])
        self.assertAlmostEqual(equality_percent(entrained, l), 1, 1)

class TestPeriodEntrained(TestCase):
    def test_period_entrained_self_cw(self):
        self.assertTrue(is_entrained_with_period(s, -1, 0.01))
        
    def test_period_entrained_lower_freq_cw(self):
        self.assertFalse(is_entrained_with_period(s, -0.9, 0.01))
        
    def test_period_entrained_neg_cw(self):
        self.assertTrue(is_entrained_with_period(s, 1, 0.01))

        
    def test_period_entrained_self_ccw(self):
        self.assertTrue(is_entrained_with_period(sr, 1, 0.01))
        
    def test_period_entrained_lower_freq_ccw(self):
        self.assertFalse(is_entrained_with_period(sr, 0.5, 0.01))
        
    def test_period_entrained_neg_ccw(self):
        self.assertTrue(is_entrained_with_period(sr, -1, 0.01))

class TestSamplesPerRevEn(TestCase):
    def test_samples_per_rev_en_cw(self):
        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(s, sv))), -100, 2)

    def test_samples_per_rev_en_phase_cw(self):
        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(s2, s2v))), -100, 2)
        
    def test_samples_per_rev_en_cos_cw(self):
        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(s3, s3v))), -100, 2)
        
    def test_samples_per_rev_en_lower_freq_cw(self):
        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(s4, s4v))), -(1/0.9*100), 1)
        self.assertNotAlmostEqual(get_samples_per_rev_energy(list(zip(s4, s4v))), -100, 1)
        
        
    def test_samples_per_rev_en_ccw(self):
        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(sr, srv))), 100, 2)
        
    def test_samples_per_rev_en_phase_ccw(self):
        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(sr2, sr2v))), 100, 2)
        
    def test_samples_per_rev_en_cos_ccw(self):
        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(sr3, sr3v))), 100, 2)
        
    def test_samples_per_rev_en_lower_freq_ccw(self):
        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(sr4, sr4v))), 1/0.9*100, 1)
        self.assertNotAlmostEqual(get_samples_per_rev_energy(list(zip(sr4, sr4v))), 100, 1)


    def test_samples_per_rev_en_dec_exp(self):
        ns = [s[i] * math.exp(-i * 0.01 * 0.55262) for i in range(len(s))]
        ns.extend(s4)
        nsv = [sv[i] * math.exp(-i * 0.01 * 0.55262) for i in range(len(sv))]
        nsv.extend(s4v)

        self.assertAlmostEqual(get_samples_per_rev_energy(list(zip(ns, nsv))), -100, 2)
        

class TestSamplesPerRev(TestCase):
    def test_samples_per_rev_cw(self):
        self.assertAlmostEqual(_get_samples_per_revolutions(list(zip(s, sv)), 8), 100, 2)

    def test_samples_per_rev_phase_cw(self):
        self.assertAlmostEqual(_get_samples_per_revolutions(list(zip(s2, s2v)), 8), 100, 0)
        
    def test_samples_per_rev_cos_cw(self):
        self.assertAlmostEqual(_get_samples_per_revolutions(list(zip(s3, s3v)), 8), 100, 2)
        
    def test_samples_per_rev_lower_freq_cw(self):
        self.assertAlmostEqual(_get_samples_per_revolutions(list(zip(s4, s4v)), 8), 1/0.9*100, 1)
        self.assertNotAlmostEqual(_get_samples_per_revolutions(list(zip(s4, s4v)), 8), 100, 1)
        

    def test_samples_per_rev_ccw(self):
        self.assertAlmostEqual(_get_samples_per_revolutions(list(zip(sr, srv)), 8), 100, 2)
        
    def test_samples_per_rev_phase_ccw(self):
        self.assertAlmostEqual(_get_samples_per_revolutions(list(zip(sr2, sr2v)), 8), 100, 0)
        
    def test_samples_per_rev_cos_ccw(self):
        self.assertAlmostEqual(_get_samples_per_revolutions(list(zip(sr3, sr3v)), 8), 100, 2)
        
    def test_samples_per_rev_lower_freq_ccw(self):
        self.assertAlmostEqual(_get_samples_per_revolutions(list(zip(sr4, sr4v)), 8), 1/0.9*100, 1)
        self.assertNotAlmostEqual(_get_samples_per_revolutions(list(zip(sr4, sr4v)), 8), 100, 1)

class TestGetWindingNumber(TestCase):
    def test_get_winding_numer_cw(self):
        self.assertAlmostEqual(_get_winding_number(list(zip(s, sv))), -100, 2)

    def test_get_winding_numer_phase_cw(self):
        self.assertAlmostEqual(_get_winding_number(list(zip(s2, s2v))), -100, 2)
        
    def test_get_winding_numer_cos_cw(self):
        self.assertAlmostEqual(_get_winding_number(list(zip(s3, s3v))), -100, 0)
        
    def test_get_winding_numer_lower_freq_cw(self):
        self.assertAlmostEqual(_get_winding_number(list(zip(s4, s4v))), -90, 2)
        self.assertNotAlmostEqual(_get_winding_number(list(zip(s4, s4v))), -100, 2)
        

    def test_get_winding_numer_ccw(self):
        self.assertAlmostEqual(_get_winding_number(list(zip(sr, srv))), 100, 2)
        
    def test_get_winding_numer_phase_ccw(self):
        self.assertAlmostEqual(_get_winding_number(list(zip(sr2, sr2v))), 100, 2)
        
    def test_get_winding_numer_cos_ccw(self):
        self.assertAlmostEqual(_get_winding_number(list(zip(sr3, sr3v))), 100, 0)
        
    def test_get_winding_numer_lower_freq_ccw(self):
        self.assertAlmostEqual(_get_winding_number(list(zip(sr4, sr4v))), 90, 2)
        self.assertNotAlmostEqual(_get_winding_number(list(zip(sr4, sr4v))), 100, 2)

class TestAngFromAmpVel(TestCase):
    def test_get_angle_from_amp_vel(self):
        for _ in range(1000):
            ang = random.random() * 2 * math.pi
            len = random.random() * 1000000
            x = math.cos(ang) * len
            y = math.sin(ang) * len

            self.assertAlmostEqual(_get_angle_from_amp_vel(x, y), ang, 2)
            
class TestHasNPeriod(TestCase):
    def test_has_n_period_self_cw(self):
        self.assertTrue(_has_N_period(s, int(1 / 0.01)))
        
    def test_has_n_period_lower_freq_cw(self):
        self.assertTrue(_has_N_period(s4, int(1 / (0.01 * 0.9))))
        self.assertFalse(_has_N_period(s4, int(1 / 0.01)))
        
        
    def test_has_n_period_self_ccw(self):
        self.assertTrue(_has_N_period(sr, int(-1 / 0.01)))
        
    def test_has_n_period_lower_freq_ccw(self):
        self.assertTrue(_has_N_period(sr4, int(-1 / (0.01 * 0.9))))
        self.assertFalse(_has_N_period(sr4, int(-1 / 0.01)))

class TestAngDiff(TestCase):
    def test_ang_diff(self):
        for _ in range(1000):
            ang1 = random.random() * math.pi
            ang2 = random.random() * math.pi

            self.assertAlmostEqual(_angle_diff(ang1, ang2, False), ang1 - ang2, 2)

    def test_ang_diff_2pi(self):
        for _ in range(1000):
            ang2 = random.random() * 100 * math.pi
            ang1 = random.random() * math.pi + math.pi + ang2

            self.assertAlmostEqual(_angle_diff(ang1, ang2), ang1 - ang2 - math.pi, 2)

    def test_ang_diff_abs(self):
        for _ in range(1000):
            ang1 = random.random() * math.pi
            ang2 = random.random() * math.pi

            self.assertAlmostEqual(_angle_diff(ang1, ang2, True), abs(ang1 - ang2), 2)

class TestMinAngDiff(TestCase):
    def test_min_ang_diff(self):
        for _ in range(1000):
            ang1 = random.random() * math.pi
            ang2 = random.random() * math.pi

            self.assertAlmostEqual(_min_angle_diff(ang1, ang2), ang1 - ang2, 2)

    def test_min_ang_diff_2pi(self):
        for _ in range(1000):
            ang1 = random.random() * math.pi + math.pi
            ang2 = -random.random() * math.pi

            self.assertAlmostEqual(_min_angle_diff(ang1, ang2), ang1 - ang2 - 2 * math.pi, 2)