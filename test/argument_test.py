from unittest import TestCase
from unittest.mock import patch
from io import StringIO
from math import sin, cos, exp

from pyno.tools import str_to_list
from .test_tools import is_list_equal_variance

from pyno import run

class TestArgument(TestCase):
    def test_out_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-o"], "")
            self.assertRegex(out.getvalue(), "^-o: No output destination given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "v", "1", "-o", "//"], "")
            self.assertRegex(out.getvalue(), "^-o: // is not a valid output path")
    
    def test_in_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-i"], "")
            self.assertRegex(out.getvalue(), "^-i: No input file given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "v", "1", "-i", "/this does and should not exist/"], "")
            self.assertRegex(out.getvalue(), "^-i: /this does and should not exist/ does not exist")

    def test_prog_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-p"], "")
            self.assertRegex(out.getvalue(), "^-p: Progress is only an option when generating Arnold tongues")

    def test_thread_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-t"], "")
            self.assertRegex(out.getvalue(), "^-t: Threads is only an option when generating Arnold tongues")

class TestArgumentSD(TestCase):
    def test_sd_eq_sin(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-e", "sin(x)", "0", "0.01", "10000"], "")
            lo = str_to_list(out.getvalue())[0]
            self.assertTrue(is_list_equal_variance(lo, [sin(t * 0.01) for t in range(10000)]))

    def test_sd_eq_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-e", "sin(x)"], "")
            self.assertEqual(out.getvalue()[0:31], "-e: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-e", "sin(x)", "0", "0.01", "abc"], "")
            self.assertEqual(out.getvalue()[0:24], "-e: Wrong argument types")
    
    def test_sd_harm_dec(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-sh", "0", "0.01", "10000", "1", "0", "1", "0.01", "0.5"], "")
            lo = str_to_list(out.getvalue())[0]
            self.assertTrue(is_list_equal_variance(lo, [cos(x * 0.01 * (0.5 ** 0.5)) * exp(-0.005 * x * 0.01) for x in range(10000)]))
    
    def test_sd_harm_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-sh"], "")
            self.assertRegex(out.getvalue(), "^-sh: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-sh", "0", "0.01", "10000", "1", "0", "1", "abc", "0.5"], "")
            self.assertRegex(out.getvalue(), "^-sh: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-sh", "0", "0.01", "10000", "1", "0", "1", "0.01", "0.5", "abc"], "")
            self.assertRegex(out.getvalue(), "^-sh: Forcing term not a function or stdin")
    
    def test_sd_van(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-sv", "0", "0.01", "10000", "1", "0", "1"], "")
            lo = str_to_list(out.getvalue())[0]
            self.assertEqual(len(lo), 10000)
    
    def test_sd_van_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-sv"], "")
            self.assertRegex(out.getvalue(), "^-sv: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-sv", "0", "0.01", "10000", "1", "abc", "1"], "")
            self.assertRegex(out.getvalue(), "^-sv: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-sv", "0", "0.01", "10000", "1", "0", "1", "fun"], "")
            self.assertRegex(out.getvalue(), "^-sv: Forcing term not a function or stdin")
    
    def test_sd_vanc(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svc", "0", "0.01", "10000", "2", "[1, 0]", "[0, 1]", "[1, 0.5]", "[1, 1]"], "")
            lo1, lo2 = str_to_list(out.getvalue())
            self.assertEqual(len(lo1), 10000)
            self.assertEqual(len(lo2), 10000)
    
    def test_sd_vanc_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svc"], "")
            self.assertRegex(out.getvalue(), "^-svc: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svc", "0", "0.01", "10000", "2", "1", "0", "1", "1"], "")
            self.assertRegex(out.getvalue(), "^-svc: Wrong argument types")
    
    def test_sd_vd(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svd", "0", "0.01", "10000", "1", "0.5", "1", "0.01", "1", "0.01", "0", "1", "sin(x)", "cos(x)"], "")
            lo1, = str_to_list(out.getvalue())
            self.assertEqual(len(lo1), 10000)
    
    def test_sd_vd_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svd"], "")
            self.assertRegex(out.getvalue(), "^-svd: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svd", "0", "0.01", "10000", "1", "0.5", "1", "0.01", "1", "0.01", "abc", "1", "sin(x)", "cos(x)"], "")
            self.assertRegex(out.getvalue(), "^-svd: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svd", "0", "0.01", "10000", "1", "0.5", "1", "0.01", "1", "0.01", "0", "1", "sin(x)", "fun"], "")
            self.assertRegex(out.getvalue(), "^-svd: Wrong argument types")
    
    def test_sd_vdc(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svdc", "0", "0.01", "10000", "2", "[1, 1j]", "[0.5, 0.1]", "[1, 10]", "[0.01, 0.1]", "[1, 3]", "[0.01, 0.05]", "[sin(x), cos(x)]", "[1, 2]"], "")
            lo1, lo2 = str_to_list(out.getvalue())
            self.assertEqual(len(lo1), 10000)
            self.assertEqual(len(lo2), 10000)
    
    def test_sd_vdc_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svdc"], "")
            self.assertRegex(out.getvalue(), "^-svdc: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svdc", "0", "0.01", "10000", "2", "[1, 1j]", "0.5", "[1, 10]", "[0.01, 0.1]", "[1, 3]", "[0.01, 0.05]", "[sin(x), cos(x)]", "[1, 2]"], "")
            self.assertRegex(out.getvalue(), "^-svdc: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "sd", "-svdc", "0", "0.01", "10000", "2", "[1, 1j]", "[0.5, 0.1]", "[1, 10]", "[0.01, 0.1]", "[1, 3]", "[0.01, 0.05]", "[fun, cos(x)]", "[1, 2]"], "")
            self.assertRegex(out.getvalue(), "^-svdc: Wrong argument types")


class TestArgumentRT(TestCase):
    def test_rt_eq_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-e"], "")
            self.assertRegex(out.getvalue(), "^-e: No equation given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-e", "sin(t)"], "")
            self.assertRegex(out.getvalue(), "^-e: Wrong argument type")
    
    def test_rt_out_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-o"], "")
            self.assertRegex(out.getvalue(), "^-o: Output cannot be defined in realtime plotting")

    def test_rt_in_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-i"], "")
            self.assertRegex(out.getvalue(), "^-i: Realtime does not support input files")
    
    def test_rt_harm_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-sh"], "")
            self.assertRegex(out.getvalue(), "^-sh: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-sh", "1", "0", "1", "abc", "0.5"], "")
            self.assertRegex(out.getvalue(), "^-sh: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-sh", "1", "0", "1", "0.01", "0.5", "abc"], "")
            self.assertRegex(out.getvalue(), "^-sh: Forcing term not a function or stdin")
    
    def test_rt_van_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-sv"], "")
            self.assertRegex(out.getvalue(), "^-sv: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-sv", "1", "abc", "1"], "")
            self.assertRegex(out.getvalue(), "^-sv: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-sv", "1", "0", "1", "fun"], "")
            self.assertRegex(out.getvalue(), "^-sv: Forcing term not a function or stdin")
    
    def test_rt_vanc_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-svc"], "")
            self.assertRegex(out.getvalue(), "^-svc: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-svc", "2", "1", "0", "1", "1"], "")
            self.assertRegex(out.getvalue(), "^-svc: Wrong argument types")
    
    def test_at_vd_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-svd"], "")
            self.assertRegex(out.getvalue(), "^-svd: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-svd", "1", "0.5", "1", "0.01", "1", "0.01", "abc", "1", "sin(x)", "cos(x)"], "")
            self.assertRegex(out.getvalue(), "^-svd: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-svd", "1", "0.5", "1", "0.01", "1", "0.01", "0", "1", "sin(x)", "fun"], "")
            self.assertRegex(out.getvalue(), "^-svd: Wrong argument types")
    
    def test_at_vdc_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-svdc"], "")
            self.assertRegex(out.getvalue(), "^-svdc: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-svdc", "2", "[1, 1j]", "0.5", "[1, 10]", "[0.01, 0.1]", "[1, 3]", "[0.01, 0.05]", "[sin(x), cos(x)]", "[1, 2]"], "")
            self.assertRegex(out.getvalue(), "^-svdc: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "rt", "0", "0.1", "-svdc", "2", "[1, 1j]", "[0.5, 0.1]", "[1, 10]", "[0.01, 0.1]", "[1, 3]", "[0.01, 0.05]", "[fun, cos(x)]", "[1, 2]"], "")
            self.assertRegex(out.getvalue(), "^-svdc: Wrong argument types")


class TestArgumentAT(TestCase):
    def test_at_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at"], "")
            self.assertRegex(out.getvalue(), "^at: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "abc", "a*sin(t*f)"], "")
            self.assertRegex(out.getvalue(), "^at: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "fun"], "")
            self.assertRegex(out.getvalue(), "^at: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)"], "")
            self.assertRegex(out.getvalue(), "^at: Differential equation to solve never given")

    def test_at_eq_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "v", "1", "-e", "sin(x)"], "")
            self.assertRegex(out.getvalue(), "^-e: Equation is not an option in Arnold Tonge")

    def test_at_thread_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "v", "1", "-t"], "")
            self.assertRegex(out.getvalue(), "^-t: No amount of threads given")

    def test_at_harm_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "h"], "")
            self.assertRegex(out.getvalue(), "^h: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "h", "1", "abc", "0.5"], "")
            self.assertRegex(out.getvalue(), "^h: Wrong argument types")

    def test_at_van_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "v"], "")
            self.assertRegex(out.getvalue(), "^v: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "v", "abc"], "")
            self.assertRegex(out.getvalue(), "^v: Wrong argument types")
    
    # TODO: Add chained Van der Pol to arguments 
#    def test_at_vanc_fail(self):
#        with patch('sys.stdout', new = StringIO()) as out:
#            run([".", "at", "0", "0.1", "-svc"], "")
#            self.assertRegex(out.getvalue(), "^-svc: Not enough parameters given")
#
#        with patch('sys.stdout', new = StringIO()) as out:
#            run([".", "at", "0", "0.1", "-svc", "2", "1", "0", "1", "1"], "")
#            self.assertRegex(out.getvalue(), "^-svc: Wrong argument types")
    
    def test_at_vd_fail(self):
        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "vd"], "")
            self.assertRegex(out.getvalue(), "^vd: Not enough parameters given")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "vd", "0.5", "1", "0.01", "1", "abc", "1", "sin(x)", "cos(x)"], "")
            self.assertRegex(out.getvalue(), "^vd: Wrong argument types")

        with patch('sys.stdout', new = StringIO()) as out:
            run([".", "at", "0.1", "1", "3", "1", "0", "3", "1", "100000", "a*sin(t*f)", "vd", "0.5", "1", "0.01", "1", "0.01", "1", "sin(x)", "fun"], "")
            self.assertRegex(out.getvalue(), "^vd: Wrong argument types")
    
    # TODO: Add chained Vilfan-Duke to arguments
#    def test_at_vdc_fail(self):
#        with patch('sys.stdout', new = StringIO()) as out:
#            run([".", "at", "0", "0.1", "-svdc"], "")
#            self.assertRegex(out.getvalue(), "^-svdc: Not enough parameters given")
#
#        with patch('sys.stdout', new = StringIO()) as out:
#            run([".", "at", "0", "0.1", "-svdc", "2", "[1, 1j]", "0.5", "[1, 10]", "[0.01, 0.1]", "[1, 3]", "[0.01, 0.05]", "[sin(x), cos(x)]", "[1, 2]"], "")
#            self.assertRegex(out.getvalue(), "^-svdc: Wrong argument types")
#
#        with patch('sys.stdout', new = StringIO()) as out:
#            run([".", "at", "0", "0.1", "-svdc", "2", "[1, 1j]", "[0.5, 0.1]", "[1, 10]", "[0.01, 0.1]", "[1, 3]", "[0.01, 0.05]", "[fun, cos(x)]", "[1, 2]"], "")
#            self.assertRegex(out.getvalue(), "^-svdc: Wrong argument types")