from unittest import TestCase
from pyno.entrainment import *
from pyno.graph import *
from pyno.solver import *
from pyno.tools import calc_ang_vel
from pyno.files import load_data_sets
from defines import ROOT_DIR
from .test_tools import *
import os

class TestArnoldTongues(TestCase):
    def test_arnold_tongue_van_der_pol(self):
        def f(f, a):
            y, dy = solve_forced_van_der_pol(0, 0.1, 10000, 1, 0, 2, lambda t : a*math.sin(f*t))
            return (y, dy, VanDerPolForcedGraph("Forced Van der Pol", 0, 0.1, [], [], 2, "a*sin(f*t)"))
		
        at1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "arnold_tongue_van_der_pol_test.bin"))
        at2 = get_arnold_tongues(f, 0, 3, 0.3, -3.001, 3, 0.3, 0.1, 1, False, 4)

        for w1, w2 in zip(at1.windings, at2.windings):
            self.assertTrue(is_list_equal_variance(w1, w2))

    def test_arnold_tongue_damped_harmonic(self):
        def f(f, a):
            y, dy = solve_forced_damped_harmonic(0, 0.1, 10000, 1, 0, 1, 10, 0.01, lambda t : a*math.sin(f*t))
            return (y, dy, HarmonicGraph("Forced Harmonic", 0, 0.1, [], [], 1, 10, 0.01))
		
        omega_0 = math.sqrt(10 / 1 - (0.01 / (2 * 1)) ** 2)

        at1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "arnold_tongue_damped_harmonic_test.bin"))
        at2 = get_arnold_tongues(f, 0, 3, 0.3, -3.001, 3, 0.3, 0.1, omega_0, False, 4)

        for w1, w2 in zip(at1.windings, at2.windings):
            self.assertTrue(is_list_equal_variance(w1, w2))

    def test_arnold_tongue_vilfan_duke(self):
        def f(f, a):
            y, dy = solve_vilfan_duke(0, 0.1, 10000, 1, 1, 10, 0.01, 3, 0.1, lambda t : a*math.sin(f*t), 1.5, lambda t : math.sin(3*t), lambda t : math.sin(t*0.3))
            return (y, dy, VilfanDukeGraph("Vilfan-Duke", 0, 0.1, [], [], 1, 10, 0.01, 3, 0.1, "a*sin(f*t)", 1.5, "sin(3*t)", "sin(t*0.3)"))
		
        y, dy, _ = f(0, 0)
        omega_0 = calc_ang_vel(zip(y, dy), 0.1)

        at1, = load_data_sets(os.path.join(ROOT_DIR, "test", "resources", "arnold_tongue_vilfan_duke_test.bin"))
        at2 = get_arnold_tongues(f, 0, 3, 0.3, -3.001, 3, 0.3, 0.1, omega_0, False, 4)

        for w1, w2 in zip(at1.windings, at2.windings):
            self.assertTrue(is_list_equal_variance(w1, w2))