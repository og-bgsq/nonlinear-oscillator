def equality_percent(l1: list[float], l2: list[float]):
    n_equal = 0
    for e1, e2 in zip(l1, l2):
        if e1 == e2:
            n_equal += 1
    
    return n_equal / len(l1)

def is_list_equal_variance(l1: list[float], l2: list[float], variance: float = 0.01) -> bool:
    for e1, e2 in zip(l1, l2):
        if not is_equal_variance(e1, e2, variance):
            raise Exception(str(e1) + ", " + str(e2))
            return False
    
    return True

def is_equal_variance(v1: float, v2: float, variance: float = 0.01) -> bool:
    return abs(v1 - v2) < variance