from unittest import TestCase
from pyno.files.load_file import *
from pyno.files.write_file import *
import os
from defines import ROOT_DIR

class TestV10(TestCase):
    def test_graph(self):
        g_in = Graph("Test", 10, 0.01, [float(i) for i in range(100000)])
        write_data_sets_v1(os.path.join(ROOT_DIR, "test.out", "test_v1_graph.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v1_graph.bin"))

        self.assertEqual(g_in.t0, g_out.t0)
        self.assertEqual(g_in.dt, g_out.dt)
        self.assertTrue(g_in.x == g_out.x)
        self.assertEqual(g_out.name, "")

    def test_graphs(self):
        g_in1 = Graph("Test", 10, 0.01, [float(i) for i in range(100000)])
        g_in2 = Graph("Test", 10, 0.01, [float(i) for i in range(100000)])
        write_data_sets_v1(os.path.join(ROOT_DIR, "test.out", "test_v1_graphs.bin"), [g_in1, g_in2])
        g_out1, g_out2 = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v1_graphs.bin"))

        self.assertEqual(g_in1.t0, g_out1.t0)
        self.assertEqual(g_in1.dt, g_out1.dt)
        self.assertTrue(g_in1.x == g_out1.x)
        self.assertEqual(g_out1.name, "")

        self.assertEqual(g_in2.t0, g_out2.t0)
        self.assertEqual(g_in2.dt, g_out2.dt)
        self.assertTrue(g_in2.x == g_out2.x)
        self.assertEqual(g_out2.name, "")

class TestV11(TestCase):
    def test_graph(self):
        g_in = Graph("Test", 10, 0.01, [float(i) for i in range(100000)], [float(i) for i in range(100000)])
        write_data_sets_v11(os.path.join(ROOT_DIR, "test.out", "test_v11_graph.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v11_graph.bin"))

        self.assertEqual(g_in.t0, g_out.t0)
        self.assertEqual(g_in.dt, g_out.dt)
        self.assertTrue(g_in.x == g_out.x)
        self.assertEqual(g_out.name, "")
        self.assertTrue(g_in.dx == g_out.dx)

    def test_graphs(self):
        g_in1 = Graph("Test", 10, 0.01, [float(i) for i in range(100000)], [float(i) for i in range(100000)])
        g_in2 = Graph("Test", 10, 0.01, [float(-i) for i in range(100000)], [float(-i) for i in range(100000)])
        write_data_sets_v11(os.path.join(ROOT_DIR, "test.out", "test_v11_graphs.bin"), [g_in1, g_in2])
        g_out1, g_out2 = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v11_graphs.bin"))

        self.assertEqual(g_in1.t0, g_out1.t0)
        self.assertEqual(g_in1.dt, g_out1.dt)
        self.assertTrue(g_in1.x == g_out1.x)
        self.assertEqual(g_out1.name, "")
        self.assertTrue(g_in1.dx == g_out1.dx)

        self.assertEqual(g_in2.t0, g_out2.t0)
        self.assertEqual(g_in2.dt, g_out2.dt)
        self.assertTrue(g_in2.x == g_out2.x)
        self.assertEqual(g_out2.name, "")
        self.assertTrue(g_in2.dx == g_out2.dx)

class TestV12(TestCase):
    def test_graph(self):
        g_in = Graph("Test", 10, 0.01, [float(i) for i in range(100000)])
        write_data_sets_v12(os.path.join(ROOT_DIR, "test.out", "test_v12_graph.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v12_graph.bin"))

        self.assertEqual(g_in.t0, g_out.t0)
        self.assertEqual(g_in.dt, g_out.dt)
        self.assertTrue(g_in.x == g_out.x)
        self.assertEqual(g_in.name, g_out.name)
        self.assertTrue(g_in.dx == g_out.dx)

    def test_graphs(self):
        g_in1 = Graph("Test", 10, 0.01, [float(i) for i in range(100000)])
        g_in2 = Graph("Test", 10, 0.01, [float(i) for i in range(100000)])
        write_data_sets_v12(os.path.join(ROOT_DIR, "test.out", "test_v12_graphs.bin"), [g_in1, g_in2])
        g_out1, g_out2 = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v12_graphs.bin"))

        self.assertEqual(g_in1.t0, g_out1.t0)
        self.assertEqual(g_in1.dt, g_out1.dt)
        self.assertTrue(g_in1.x == g_out1.x)
        self.assertEqual(g_in1.name, g_out1.name)
        self.assertTrue(g_in1.dx == g_out1.dx)

        self.assertEqual(g_in2.t0, g_out2.t0)
        self.assertEqual(g_in2.dt, g_out2.dt)
        self.assertTrue(g_in2.x == g_out2.x)
        self.assertEqual(g_in2.name, g_out2.name)
        self.assertTrue(g_in2.dx == g_out2.dx)

    def test_van_der_pol(self):
        g_in = VanDerPolGraph("Test Van der Pol", 10, 0.01, [float(i) for i in range(100000)], [float(i) for i in range(100000)], 20)
        write_data_sets_v12(os.path.join(ROOT_DIR, "test.out", "test_v12_van_der_pol.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v12_van_der_pol.bin"))

        self.assertEqual(g_in.t0, g_out.t0)
        self.assertEqual(g_in.dt, g_out.dt)
        self.assertTrue(g_in.x == g_out.x)
        self.assertEqual(g_in.name, g_out.name)
        self.assertTrue(g_in.dx == g_out.dx)
        self.assertEqual(g_in.mu, g_out.mu)

    def test_van_der_pol_forced(self):
        g_in = VanDerPolForcedGraph("Test Van der Pol Forced", 10, 0.01, [float(i) for i in range(100000)], [float(i) for i in range(100000)], 20, "This is a tottally real forcing function")
        write_data_sets_v12(os.path.join(ROOT_DIR, "test.out", "test_v12_van_der_pol_forced.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v12_van_der_pol_forced.bin"))

        self.assertEqual(g_in.t0, g_out.t0)
        self.assertEqual(g_in.dt, g_out.dt)
        self.assertTrue(g_in.x == g_out.x)
        self.assertEqual(g_in.name, g_out.name)
        self.assertTrue(g_in.dx == g_out.dx)
        self.assertEqual(g_in.mu, g_out.mu)
        self.assertEqual(g_in.F, g_out.F)

    def test_van_der_pol_coupled(self):
        g_in = VanDerPolCoupledGraph("Test Van der Pol Coupled", 10, 0.01, [float(i) for i in range(100000)], [float(i) for i in range(100000)], 20, 1.5)
        write_data_sets_v12(os.path.join(ROOT_DIR, "test.out", "test_v12_van_der_pol_coupled.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v12_van_der_pol_coupled.bin"))

        self.assertEqual(g_in.t0, g_out.t0)
        self.assertEqual(g_in.dt, g_out.dt)
        self.assertTrue(g_in.x == g_out.x)
        self.assertEqual(g_in.name, g_out.name)
        self.assertTrue(g_in.dx == g_out.dx)
        self.assertEqual(g_in.mu, g_out.mu)
        self.assertEqual(g_in.k, g_out.k)

    def test_vilfan_duke(self):
        g_in = VilfanDukeGraph("Test Vilfan and Duke", 10, 0.1, [i for i in range(100000)], [i for i in range(100000)], 0.3, 0.01, 3.4, 0.56, 1.2, "This is a real forcing term", 1.5, "This is the left function", "This is the right function")
        write_data_sets_v12(os.path.join(ROOT_DIR, "test.out", "test_v12_vilfan_duke.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v12_vilfan_duke.bin"))

        self.assertEqual(g_in.t0, g_out.t0)
        self.assertEqual(g_in.dt, g_out.dt)
        self.assertTrue(g_in.x == g_out.x)
        self.assertEqual(g_in.name, g_out.name)
        self.assertTrue(g_in.dx == g_out.dx)
        self.assertEqual(g_in.omegaj, g_out.omegaj)
        self.assertEqual(g_in.epsilonj, g_out.epsilonj)
        self.assertEqual(g_in.dr, g_out.dr)
        self.assertEqual(g_in.di, g_out.di)
        self.assertEqual(g_in.B, g_out.B)
        self.assertEqual(g_in.zeta, g_out.zeta)
        self.assertEqual(g_in.left, g_out.left)
        self.assertEqual(g_in.right, g_out.right)

    def test_harmonic(self):
        g_in = HarmonicGraph("Test Harmonic", 10, 0.1, [i for i in range(100000)], [i for i in range(100000)], 1, 3, 0.01)
        write_data_sets_v12(os.path.join(ROOT_DIR, "test.out", "test_v12_harmonic.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v12_harmonic.bin"))

        self.assertEqual(g_in.t0, g_out.t0)
        self.assertEqual(g_in.dt, g_out.dt)
        self.assertTrue(g_in.x == g_out.x)
        self.assertEqual(g_in.name, g_out.name)
        self.assertTrue(g_in.dx == g_out.dx)
        self.assertEqual(g_in.m, g_out.m)
        self.assertEqual(g_in.k, g_out.k)
        self.assertEqual(g_in.c, g_out.c)

class TestV13(TestCase):
    def test_arnold_tongue(self):
        g_in = ArnoldTongues([[i * j for j in range(100)] for i in range(100)], [[VanDerPolForcedGraph("Forcing", 10, 0.1, [], [], 10, "a*sin(t*f)")]*100]*100, 3, 0, 0.01, 3, 0, 0.01, 1)
        write_data_sets_v13(os.path.join(ROOT_DIR, "test.out", "test_v13_arnold_tongue.bin"), [g_in])
        g_out, = load_data_sets(os.path.join(ROOT_DIR, "test.out", "test_v13_arnold_tongue.bin"))

        self.assertEqual(g_in.a_max, g_out.a_max)
        self.assertEqual(g_in.a_min, g_out.a_min)
        self.assertEqual(g_in.da, g_out.da)
        self.assertEqual(g_in.f_max, g_out.f_max)
        self.assertEqual(g_in.f_min, g_out.f_min)
        self.assertEqual(g_in.df, g_out.df)
        self.assertEqual(g_in.omega_0, g_out.omega_0)
        self.assertTrue(g_in.windings == g_out.windings)
        self.assertTrue(g_in.points == g_out.points)