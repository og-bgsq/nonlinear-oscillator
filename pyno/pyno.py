import sys
import math
from numpy import array
import os
from enum import Enum
from dataclasses import dataclass
from types import FunctionType

from .entrainment import get_arnold_tongues, ArnoldTongues
from .arguments import parse, ArgumentDisp, ArgumentEq, ArgumentHarm, ArgumentHelp, ArgumentIn, ArgumentOut, ArgumentVan, ArgumentVanC, ArgumentVilDuke, ArgumentVilDukeC, ArgumentProg, ArgumentThread, ProgramState, ArgumentArnold, ArgumentSolver, ArgumentContour
from .graph import graph_from_equation, Graph, VanDerPolGraph, VilfanDukeGraph, HarmonicGraph, VanDerPolForcedGraph, VanDerPolCoupledGraph
from .files import load_data_sets, write_data_sets_v14
from .solver import solve_damped_harmonic, solve_forced_damped_harmonic, solve_van_der_pol, solve_forced_van_der_pol, solve_fa_forced_van_der_pol, solve_van_der_pol_coupled, solve_vilfan_duke, solve_vilfan_duke_chain, get_damped_harmonic_rt, get_forced_damped_harmonic_rt, get_van_der_pol_rt, get_forced_van_der_pol_rt, get_vilfan_duke_rt, list_to_force, solver
from .graphics import plot_time_phase, FuncType, plot_time_phase_realtime, write_image, plot_arnold_tongues
from .help import helpscreen
from .tools import parse_stdin, calc_ang_vel

from .solver import solve_ode as sol

class EquationType(Enum):
    EQUATION = 0
    HARMONIC = 1
    VAN_DER_POL = 2
    VAN_DER_POL_COUPLED = 3
    VILFAN_DUKE = 4
    VILFAN_DUKE_COUPLED = 5

@dataclass
class Equation:
    equation_type: EquationType
    data: list
    t0: float
    dt: float
    N: int

    def __init__(self, *args):
        if len(args) == 5:
            self._init_sd(*args)
        elif len(args) == 2:
            self._init_rt(*args)

    def _init_rt(self, eqtype: EquationType, data: list):
        self.data = data
        self.equation_type = eqtype
        self.t0 = self.dt = self.N = None

    def _init_sd(self, eqtype: EquationType, t0: float, dt: float, N: int, data: list):
        self.data = data
        self.equation_type = eqtype
        self.t0 = t0
        self.dt = dt
        self.N = N

def run(args, input):
    stdin = parse_stdin(input)
    parsed_data = parse(args[1:])
    if not parsed_data:
        helpscreen(args[0])
        return
    program_state, parsedArgs = parsed_data

    outputFile = None
    inputFile = None

    equations = []
    contour = []

    dispGraph = False

    progress = False
    threads = 1

    # Go through all arguments and try to match its type (if the argument is even valid), if a match is found,
    # then we update whatever values is needed to be updated by that argument
    for arg in parsedArgs:
        match arg.__class__.__qualname__:
            case ArgumentHelp.__qualname__:
                helpscreen(args[0])
                return
            case ArgumentOut.__qualname__:
                outputFile = arg.args[0]
            case ArgumentIn.__qualname__:
                inputFile = arg.args[0]
            case ArgumentSolver.__qualname__:
                sol.solver = arg.args[0]
            case ArgumentContour.__qualname__:
                contour = arg.args[0]
            case ArgumentEq.__qualname__:
                if program_state == ProgramState.RT:
                    eq = Equation(EquationType.EQUATION, [arg.args[0]])
                else:
                    eq = Equation(EquationType.EQUATION, *arg.args[2:5], arg.args[0:2])
                
                equations.append(eq)
            case ArgumentDisp.__qualname__:
                dispGraph = True
            case ArgumentHarm.__qualname__:
                if program_state == ProgramState.AT:
                    F_str = parsedArgs[0].args[-2]
                    F = parsedArgs[0].args[-1]
                    equations.append(Equation(EquationType.HARMONIC, [arg.args[0], arg.args[1], arg.args[2], F_str, F]))
                    continue

                F_str = arg.args[-2]
                F = None
                if arg.args[-1] == "-":
                    if program_state != ProgramState.RT:
                        F = list_to_force(stdin[0], arg.args[0], arg.args[1])
                    else:
                        F = list_to_force(stdin[0], parsedArgs[0].args[0], parsedArgs[0].args[1])
                elif arg.args[-1]:
                    F = arg.args[-1]

                if program_state == ProgramState.RT:
                    eq = Equation(EquationType.HARMONIC, [*arg.args[0:5], F_str, F])
                else:
                    eq = Equation(EquationType.HARMONIC, *arg.args[0:3], [*arg.args[3:8], F_str, F])
                
                equations.append(eq)
            case ArgumentVan.__qualname__:
                if program_state == ProgramState.AT:
                    F_str = parsedArgs[0].args[-2]
                    F = parsedArgs[0].args[-1]
                    equations.append(Equation(EquationType.VAN_DER_POL, [arg.args[0], F_str, F]))
                    continue

                F_str = arg.args[-2]
                F = None
                if arg.args[-1] == "-":
                    if program_state != ProgramState.RT:
                        F = list_to_force(stdin[0], arg.args[0], arg.args[1])
                    else:
                        F = list_to_force(stdin[0], float(parsedArgs[0].args[0]), float(parsedArgs[0].args[1]))
                elif arg.args[-1]:
                    F = arg.args[-1]

                if program_state == ProgramState.RT:
                    eq = Equation(EquationType.VAN_DER_POL, [arg.args[0], arg.args[1], arg.args[2], F_str, F])
                else:
                    eq = Equation(EquationType.VAN_DER_POL, arg.args[0], arg.args[1], arg.args[2], [arg.args[3], arg.args[4], arg.args[5], F_str, F])
                
                equations.append(eq)
            case ArgumentVanC.__qualname__:
                if program_state == ProgramState.RT:
                    eq = Equation(EquationType.VAN_DER_POL_COUPLED, arg.args)
                else:
                    eq = Equation(EquationType.VAN_DER_POL_COUPLED, *arg.args[0:3], arg.args[3:])

                equations.append(eq)
            case ArgumentVilDuke.__qualname__:
                if program_state == ProgramState.AT:
                    F_str = parsedArgs[0].args[-2]
                    F = parsedArgs[0].args[-1]
                    equations.append(Equation(EquationType.VILFAN_DUKE, [*arg.args, F_str, F]))
                    continue

                if program_state == ProgramState.RT:
                    eq = Equation(EquationType.VILFAN_DUKE, arg.args)
                else:
                    eq = Equation(EquationType.VILFAN_DUKE, *arg.args[0:3], arg.args[3:])
                
                equations.append(eq)
            case ArgumentVilDukeC.__qualname__:
                if program_state == ProgramState.RT:
                    eq = Equation(EquationType.VILFAN_DUKE_COUPLED, arg.args)
                else:
                    eq = Equation(EquationType.VILFAN_DUKE_COUPLED, *arg.args[0:3], arg.args[3:])
                
                equations.append(eq)
            case ArgumentProg.__qualname__:
                progress = True

            case ArgumentThread.__qualname__:
                threads = arg.args[0]

    match program_state:
        case ProgramState.RT:
            _realtime_loop(*parsedArgs[0].args[0:2], equations)
        case ProgramState.SD:
            _standard_loop(outputFile, inputFile, dispGraph, equations)
        case ProgramState.AT:
            if parsedArgs[0].__class__.__qualname__ == ArgumentArnold.__qualname__:
                _arnoldtongue_loop(outputFile, inputFile, contour, *parsedArgs[0].args[0:8], equations[0], progress, threads)
            else:
                _arnoldtongue_loop(outputFile, inputFile, contour)


def _arnoldtongue_loop(outputFile: str, inputFile: str, contour: list[float], dt: float = None, min_freq: float = None, max_freq: float = None, step_freq: float = None, min_amp: float = None, max_amp: float = None, step_amp: float = None, N: int = None, eq_type: Equation = None, progress: bool = None, threads: int = None):
    omega_0 = 1

    graphs = []

    if inputFile:
        graphs = load_data_sets(inputFile)
    
    if eq_type:
        match eq_type.equation_type:
            case EquationType.HARMONIC:
                def f(f, a):
                    y, dy = solve_forced_damped_harmonic(0, dt, N, 1, 0, *eq_type.data[0:3], lambda t : eq_type.data[-1](f, a, t))
                    return (y, dy, HarmonicGraph("Forced Harmonic", 0, dt, [], [], eq_type.data[0], eq_type.data[1], eq_type.data[2]))
                
                omega_0 = math.sqrt(eq_type.data[1] / eq_type.data[0] - (eq_type.data[2] / (2 * eq_type.data[0])) ** 2)

            case EquationType.VAN_DER_POL:

                if eq_type.data[-1]:
                    def f(f, a):
                        y, dy = solve_forced_van_der_pol(0, dt, N, 1, 0, *eq_type.data[0:1], lambda t : eq_type.data[-1](f, a, t))
                        return (y, dy, VanDerPolForcedGraph("Forced Van der Pol", 0, dt, [], [], eq_type.data[0], eq_type.data[1]))
                else:
                    def f(f, a):
                        y, dy = solve_fa_forced_van_der_pol(0, dt, N, 1, 0, *eq_type.data[0:1], a, f)
                        return (y, dy, VanDerPolForcedGraph("fa-Forced Van der Pol", 0, dt, [], [], eq_type.data[0], "".join([str(eq_type.data[1]), "*sin(", str(eq_type.data[2]), "*t)"])))

                omega_0 = 1

            case EquationType.VILFAN_DUKE:
                def f(f, a):
                    y, dy = solve_vilfan_duke(0, dt, N, 1, *eq_type.data[0:5], lambda t : eq_type.data[-1](f, a, t), eq_type.data[5], eq_type.data[7], eq_type.data[9])
                    return (y, dy, VilfanDukeGraph("Vilfan-Duke", 0, dt, [], [], *eq_type.data[0:5], eq_type.data[-2], eq_type.data[5], eq_type.data[6], eq_type.data[8]))
                
                y, dy = solve_vilfan_duke(0, dt, N, 1, *eq_type.data[0:5], lambda _ : 0, eq_type.data[5], eq_type.data[7], eq_type.data[9])
                omega_0 = calc_ang_vel(zip(y, dy), dt)

        at = get_arnold_tongues(f, min_amp, max_amp, step_amp, min_freq, max_freq, step_freq, dt, omega_0, progress, threads)

        plot_arnold_tongues(at, contour)

    for g in graphs:
        if isinstance(g, ArnoldTongues):
            plot_arnold_tongues(g, contour)

    if eq_type:
        graphs.append(at)

    if outputFile:
        write_data_sets_v14(outputFile, graphs)
    else:
        _save_data(graphs)

def _realtime_loop(t0: float, dt: float, equations: list[Equation]):
    equation_functions = []

    for eq in equations:
        match eq.equation_type:
            case EquationType.EQUATION:
                equation = eq.data[1]

                equation_functions.append((equation, FuncType.TIME_DEP_WO_D))
            
            case EquationType.HARMONIC:
                y0, dy0, M, c, k, F_str, F = eq.data

                if F:
                    equation_functions.append((get_forced_damped_harmonic_rt(t0, y0, dy0, M, c, k, F), FuncType.DELTA_T_DEP))
                else:
                    equation_functions.append((get_damped_harmonic_rt(t0, y0, dy0, M, c, k), FuncType.DELTA_T_DEP))

            case EquationType.VAN_DER_POL:
                y0, dy0, mu, F_str, F = eq.data

                if F:
                    equation_functions.append((get_forced_van_der_pol_rt(t0, y0, dy0, mu, F), FuncType.DELTA_T_DEP))
                else:
                    equation_functions.append((get_van_der_pol_rt(t0, y0, dy0, mu), FuncType.DELTA_T_DEP))
            
            case EquationType.VILFAN_DUKE:
                y0, Kj, Mj, Gammaj, k, gamma, zeta_str, zeta, B, left_str, left, right_str, right = eq.data

                equation_functions.append((get_vilfan_duke_rt(t0, y0, Kj, Mj, Gammaj, k, gamma, zeta, B, left, right), FuncType.DELTA_T_DEP))

    plot_time_phase_realtime(equation_functions, t0, dt, num_vals=100)

def _standard_loop(outputFile: str, inputFile: str, dispGraph: bool, equations: list[Equation]):
    graphs = []

    # ARGUMENT EXECUTION
    # If the input file is specified, then we need to get the graphs contained in the file and add them to the graphs list
    if inputFile:
        graphs.extend(load_data_sets(inputFile))

    # If there are any elements in the equations list, then we need to convert those equations into a graphs and add them to the graphs list
    for eq in equations:
        match eq.equation_type:
            case EquationType.EQUATION:
                equation = eq.data[0]

                graphs.append(graph_from_equation(equation, eq.t0, eq.dt, eq.N))
            
            case EquationType.HARMONIC:
                # Determines whether the harmonic function has a forcing term or not
                y0, dy0, M, c, k, F_str, F = eq.data

                if F:
                    y,dy = solve_forced_damped_harmonic(eq.t0, eq.dt, eq.N, y0, dy0, M, c, k, F)
                else:
                    y,dy = solve_damped_harmonic(eq.t0, eq.dt, eq.N, y0, dy0, M, c, k)
                graphs.append(HarmonicGraph("Damped Harmonic", eq.t0, eq.dt, y, dy, M, c, k))
            
            case EquationType.VAN_DER_POL:
                # Determines whether the Van der Pol oscillatior has a forcing term or not
                y0, dy0, mu, F_str, F = eq.data

                if F:
                    y,dy = solve_forced_van_der_pol(eq.t0, eq.dt, eq.N, y0, dy0, mu, F)
                    graphs.append(VanDerPolForcedGraph("Van der Pol", eq.t0, eq.dt, y, dy, mu, F_str))
                else:
                    y,dy = solve_van_der_pol(eq.t0, eq.dt, eq.N, y0, dy0, mu)
                    graphs.append(VanDerPolGraph("Van der Pol", eq.t0, eq.dt, y, dy, mu))

            case EquationType.VAN_DER_POL_COUPLED:
                M, y0, dy0, mu, k = eq.data

                sol = solve_van_der_pol_coupled(eq.t0, eq.dt, eq.N, y0, dy0, mu, k, M)

                itr = iter(sol)

                for (y, dy, mu, k) in zip(itr, itr, mu, k):
                    graphs.append(VanDerPolCoupledGraph("Coupled Van der Pol", eq.t0, eq.dt, y, dy, mu, k))
            
            case EquationType.VILFAN_DUKE:
                y0, Kj, Mj, Gammaj, k, gamma, zeta_str, zeta, B, left_str, left, right_str, right = eq.data

                y,dy = solve_vilfan_duke(eq.t0, eq.dt, eq.N, y0, Kj, Mj, Gammaj, k, gamma, zeta, B, left, right)
                graphs.append(VilfanDukeGraph("Vilfan and Duke", eq.t0, eq.dt, y, dy, Kj, Mj, Gammaj, k, gamma, zeta_str, B, left_str, right_str))

            case EquationType.VILFAN_DUKE_COUPLED:
                M, y0, Kj, Mj, Gammaj, k, gamma, zeta_str, zeta, B = eq.data

                sol = solve_vilfan_duke_chain(eq.t0, eq.dt, eq.N, M, y0, Kj, Mj, Gammaj, k, gamma, zeta, B)

                graphs.extend([VilfanDukeGraph("Coupled Vilfan and Duke", eq.t0, eq.dt, [s[0].real for s in sol[i]], [s[0].imag for s in sol[i]], Kj[i], Mj[i], Gammaj[i], k[i], gamma[i], zeta_str[i], B[i]) for i in range(len(sol))])


    # If the dispGraph boolean is set, then we need to display the graphs
    if dispGraph:
        plot_time_phase(graphs)
    
    # If an output file is specified, then we save the graphs to that file,
    # else we just print the graphs to the terminal
    if outputFile:
        write_data_sets_v14(outputFile, graphs)
    elif not dispGraph:
        for g in graphs:
            _write(g.x)
    else:
        _save_data(graphs)

def _save_data(graphs: list[Graph | ArnoldTongues]):
    while True:
        if _ask_yn_question("Do you want to save this data?", False):
            out = input("Define output destination: ")

            # If the path ends in .bin or .png, then cut it off, so we can add it later
            if out.endswith(".bin") or out.endswith(".png") or out.endswith(".svg"):
                out = out[:-4]

            # If file at location exists, then we should ask the user if it is ment to be overwritten (if not, then we continue to start of while loop and start over)
            if os.path.exists(out + ".bin") or os.path.exists(out + ".png") or os.path.exists(out + ".svg"):
                if not _ask_yn_question("File already exists at location, do you want to overwrite it?", False):
                    continue

            # Split the path into its folder and the file name
            folder, _, file = out.rpartition(os.sep)

            # If the folder does not exists, then ask the user if we should create it for them
            if not os.path.exists(folder):
                if not _ask_yn_question("Folder does not exists, do you want to create it?", True):
                    continue

                os.mkdir(folder)

            # Write the binary data and images to the specified location (we are shure is exists, so we do not need to ehck it)
            write_data_sets_v14(out + ".bin", graphs)
            write_image(out)
        
        return

def _ask_yn_question(question: str, default: bool):
    while True:
        answer = input(question + (" [Y/n] " if default else " [y/N] "))

        if answer.lower() == "y":
            return True
        elif answer.lower() == "n":
            return False
        elif answer == "":
            return default

def _write(l: list[float]):
    sys.stdout.write("[")
    for e in l:
        sys.stdout.write(str(e))
        sys.stdout.write(",")
    sys.stdout.write("]")
