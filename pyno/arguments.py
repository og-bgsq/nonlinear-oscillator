from dataclasses import dataclass
from enum import Enum
import os
import re
from .graph import create_function, create_fa_function, graph_from_equation
from .tools import str_to_list
from .graph.math import SinLUT

class ArgType(Enum):
    HELP = 1,
    OUTPUT = 2,
    INPUT = 3,
    EQUATION = 4,
    GRAPH = 5,
    SOLVE_HARMONIC = 6,
    SOLVE_POL = 7,
    SOLVE_POLC = 8,
    SOLVE_VD = 9,
    SOLVE_VDC = 10,
    RT = 11,
    AT = 12,
    PROGRESS = 13,
    THREADS = 14,

class Argument:
    pass

class ArgumentHelp(Argument):
    pass

    def __init__(self): pass

class ArgumentOut(Argument):
    num_args = 1

    def __init__(self, program_state, *params):
        if program_state == ProgramState.RT:
            raise Exception("Output cannot be defined in realtime plotting")
        if len(params) < 1:
            raise Exception("No output destination given")
        
        if not os.access(os.path.dirname(params[0]), os.W_OK):
            raise Exception(params[0] + " is not a valid output path")

        self.args = [params[0]]
    
class ArgumentIn(Argument):
    num_args = 1
    
    def __init__(self, program_state, *params):
        if program_state == ProgramState.RT:
            raise Exception("Realtime does not support input files")
        if len(params) < 1:
            raise Exception("No input file given")
        if not os.path.exists(os.path.dirname(params[0])):
            raise Exception(params[0] + " does not exist")
        
        self.args = [params[0]]
    
class ArgumentProg(Argument):
    num_args = 0

    def __init__(self, program_state):
        if not program_state == ProgramState.AT:
            raise Exception("Progress is only an option when generating Arnold tongues")
        
class ArgumentThread(Argument):
    num_args = 1

    def __init__(self, program_state, *params):
        if program_state == ProgramState.AT:
            if len(params) < 1:
                raise Exception("No amount of threads given")
            try:
                self.args = [int(params[0])]
            except:
                raise Exception("Wrong argument type")
        else:
            raise Exception("Threads is only an option when generating Arnold tongues")
    
class ArgumentEq(Argument):

    def __init__(self, program_state, *params):
        if program_state == ProgramState.RT:
            if len(params) < 1:
                raise Exception("No equation given")
            try:
                self.args = [params[0], create_function(params[0])]
                self.num_args = 1
            except:
                raise Exception("Wrong argument type")
        elif program_state == ProgramState.SD:
            if len(params) < 4:
                raise Exception("Not enough parameters given")
            try:
                self.args = [params[0], create_function(params[0]), float(params[1]), float(params[2]), int(params[3])]
                self.num_args = 4
            except:
                raise Exception("Wrong argument types")
        else:
            raise Exception("Equation is not an option in Arnold Tonge")
    
class ArgumentDisp(Argument):
    num_args = 0

    def __init__(self, program_state):
        if program_state == ProgramState.RT:
            raise Exception("Display is not an option in realtime")
        elif program_state == ProgramState.AT:
            raise Exception("Display is not an option in Arnold Tongue")
        
class ArgumentHarm(Argument):

    def __init__(self, program_state, *params):
        if program_state == ProgramState.RT:
            if len(params) < 5:
                raise Exception("Not enough parameters given")
            #[y0] [dy0] [M] [c] [k]
            try:
                self.args = [float(params[0]), float(params[1]), float(params[2]), float(params[3]), float(params[4])]
            except:
                raise Exception("Wrong argument types")
            
            #Check if params list is shorter than 6 and therefore does not have a forcing term, or match with a regular expression to exclude arguments from getting turned into function
            if len(params) < 6 or re.match("\\-.+", params[5]):
                self.args.append(None)
                self.num_args = 5
                return

            try:
                self.args.append(create_function(params[5]))
                self.num_args = 6
            except:
                if params[5] == "-":
                    self.args.append(params[5])
                    self.num_args = 6
                else:
                    raise Exception("Forcing term not a function or stdin")

        elif program_state == ProgramState.AT:
            if len(params) < 3:
                raise Exception("Not enough parameters given")
            #[M] [c] [k]
            try:
                self.args = [float(params[0]), float(params[1]), float(params[2])]
                self.num_args = 3
            except:
                raise Exception("Wrong argument types")

        else:
            if len(params) < 8:
                raise Exception("Not enough parameters given")
            #[t0] [dt] [N] [y0] [dy0] [M] [c] [k]
            try:
                self.args = [float(params[0]), float(params[1]), int(params[2]), float(params[3]), float(params[4]), float(params[5]), float(params[6]), float(params[7])]
            except:
                raise Exception("Wrong argument types")
            
            #Check if params list is shorter than 9 and therefore does not have a forcing term, or match with a regular expression to exclude arguments from getting turned into function
            if len(params) < 9 or re.match("\\-.+", params[8]):
                self.args.append(None)
                self.num_args = 8
                return

            try:
                self.args.append(create_function(params[8]))
                self.num_args = 9
            except:
                if params[8] == "-":
                    self.args.append(params[8])
                    self.num_args = 9
                else:
                    raise Exception("Forcing term not a function or stdin")
            
class ArgumentVan(Argument):

    def __init__(self, program_state, *params):
        if program_state == ProgramState.RT:
            if len(params) < 3:
                raise Exception("Not enough parameters given")
            #[y0] [dy0] [mu]
            try:
                self.args = [float(params[0]), float(params[1]), float(params[2])]
            except:
                raise Exception("Wrong argument types")
            
            #Check if params list is shorter than 4 and therefore does not have a forcing term, or match with a regular expression to exclude arguments from getting turned into function
            if len(params) < 4 or re.match("\\-.+", params[3]):
                self.args.append(None)
                self.num_args = 3
                return

            try:
                self.args.append(params[3])
                self.args.append(create_function(params[3]))
                self.num_args = 4
            except:
                if params[3] == "-":
                    self.args.append(params[3])
                    self.num_args = 4
                else:
                    raise Exception("Forcing term not a function or stdin")

        elif program_state == ProgramState.AT:
            if len(params) < 1:
                raise Exception("Not enough parameters given")
            #[mu]
            try:
                self.args = [float(params[0])]
                self.num_args = 1
            except:
                raise Exception("Wrong argument types")
            
        else:
            if len(params) < 6:
                raise Exception("Not enough parameters given")
            #[t0] [dt] [N] [y0] [dy0] [mu]
            try:
                self.args = [float(params[0]), float(params[1]), int(params[2]), float(params[3]), float(params[4]), float(params[5])]
            except:
                raise Exception("Wrong argument types")
            
            #Check if params list is shorter than 7 and therefore does not have a forcing term, or match with a regular expression to exclude arguments from getting turned into function
            if len(params) < 7 or re.match("\\-.+", params[6]):
                self.args.append(None)
                self.num_args = 6
                return
            
            try:
                self.args.append(params[6])
                self.args.append(create_function(params[6]))
                self.num_args = 7
            except:
                if params[6] == "-":
                    self.args.append(params[6])
                    self.args.append(params[6])
                    self.num_args = 7
                else:
                    raise Exception("Forcing term not a function or stdin")
            
class ArgumentVanC(Argument):

    def __init__(self, program_state, *params):
        if program_state == ProgramState.RT:
            if len(params) < 5:
                raise Exception("Not enough parameters given")
            #[M] [y0] [dy0] [mu] [k]
            try:
                self.args = [int(params[0]), str_to_list(params[1], should_fail_on_error = True)[0], str_to_list(params[2], should_fail_on_error = True)[0], str_to_list(params[3], should_fail_on_error = True)[0], str_to_list(params[4], should_fail_on_error = True)[0]]
                self.num_args = 5
            except:
                raise Exception("Wrong argument types")
            
        else:
            # TODO: Make "k" left-right dependant, such that the list of "k"'s contains how the oscillators are coupled to each neighbor and not just to both neighbors at once
            if len(params) < 8:
                raise Exception("Not enough parameters given")
            #[t0] [dt] [N] [M] [y0] [dy0] [mu] [k]
            try:
                self.args = [float(params[0]), float(params[1]), int(params[2]), int(params[3]), str_to_list(params[4], should_fail_on_error = True)[0], str_to_list(params[5], should_fail_on_error = True)[0], str_to_list(params[6], should_fail_on_error = True)[0], str_to_list(params[7], should_fail_on_error = True)[0]]
                self.num_args = 8
            except:
                raise Exception("Wrong argument types")
            
class ArgumentVilDuke(Argument):

    def __init__(self, program_state, *params):
        if program_state == ProgramState.RT:
            if len(params) < 10:
                raise Exception("Not enough parameters given")
            #[y0] [Kj] [Mj] [Gammaj] [k] [gamma] [zeta] [B] [left] [right]
            try:
                self.args = [float(params[0]), float(params[1]), float(params[2]), float(params[3]), float(params[4]), float(params[5]), params[6], create_function(params[6]), float(params[7]), params[8], create_function(params[8]), params[9], create_function(params[9])]
                self.num_args = 10
            except:
                raise Exception("Wrong argument types")
            
        elif program_state == ProgramState.AT:
            if len(params) < 8:
                raise Exception("Not enough parameters given")
            #[Kj] [Mj] [Gammaj] [k] [gamma] [B] [left] [right]
            try:
                self.args = [float(params[0]), float(params[1]), float(params[2]), float(params[3]), float(params[4]), float(params[5]), params[6], create_function(params[6]), params[7], create_function(params[7])]
                self.num_args = 8
            except:
                raise Exception("Wrong argument types")

        else:
            if len(params) < 13:
                raise Exception("Not enough parameters given")
            #[t0] [dt] [N] [y0] [Kj] [Mj] [Gammaj] [k] [gamma] [zeta] [B] [left] [right]
            try:
                self.args = [float(params[0]), float(params[1]), int(params[2]), complex(params[3]), float(params[4]), float(params[5]), float(params[6]), float(params[7]), float(params[8]), params[9], create_function(params[9]), float(params[10]), params[11], create_function(params[11]), params[12], create_function(params[12])]
                self.num_args = 13
            except:
                raise Exception("Wrong argument types")
            
class ArgumentVilDukeC(Argument):

    def __init__(self, program_state, *params):
        # TODO: Check if length of lists are correct
        if program_state == ProgramState.RT:
            if len(params) < 9:
                raise Exception("Not enough parameters given")
            #[M] [y0] [Kj] [Mj] [Gammaj] [k] [gamma] [zeta] [B]
            try:
                self.args = [int(params[0]), str_to_list(params[1], complex, True)[0], str_to_list(params[2], should_fail_on_error = True)[0], str_to_list(params[3], should_fail_on_error = True)[0], str_to_list(params[4], should_fail_on_error = True)[0], str_to_list(params[5], should_fail_on_error = True)[0], str_to_list(params[6], should_fail_on_error = True)[0], str_to_list(params[7], str, True)[0], str_to_list(params[7], create_function, True)[0], str_to_list(params[8], should_fail_on_error = True)[0]]
                self.num_args = 9
            except:
                raise Exception("Wrong argument types")
            
        else:
            if len(params) < 12:
                raise Exception("Not enough parameters given")
            #[t0] [dt] [N] [M] [y0] [Kj] [Mj] [Gammaj] [k] [gamma] [zeta] [B]
            try:
                self.args = [float(params[0]), float(params[1]), int(params[2]), int(params[3]), str_to_list(params[4], complex, True)[0], str_to_list(params[5], should_fail_on_error = True)[0], str_to_list(params[6], should_fail_on_error = True)[0], str_to_list(params[7], should_fail_on_error = True)[0], str_to_list(params[8], should_fail_on_error = True)[0], str_to_list(params[9], should_fail_on_error = True)[0], str_to_list(params[10], str, True)[0], str_to_list(params[10], create_function, True)[0], str_to_list(params[11], should_fail_on_error = True)[0]]
                self.num_args = 12
            except:
                raise Exception("Wrong argument types")
    
class ArgumentArnold(Argument):

    def __init__(self, program_state, *params):
        if len(params) < 9:
            raise Exception("Not enough parameters given")
        
        try:
            dt = float(params[0])
            min_freq = float(params[1])
            max_freq = float(params[2])
            step_freq = float(params[3])
            min_amp = float(params[4])
            max_amp = float(params[5])
            step_amp = float(params[6])
            N = int(params[7])
            entrainment = params[8]
            
            if entrainment == "fa":
                self.args = [dt, min_freq, max_freq, step_freq, min_amp, max_amp, step_amp, N, None, None]
            else:
                sin_lut = SinLUT(min_freq, max_freq, step_freq, 0, dt, N) if params[8] == "sin" else None

                self.args = [dt, min_freq, max_freq, step_freq, min_amp, max_amp, step_amp, N, entrainment, create_fa_function(entrainment, sin_lut)]
            
            self.num_args = 9
        except:
            raise Exception("Wrong argument types")
    
class ArgumentRealtime(Argument):

    def __init__(self, *params):
        if len(params) < 2:
            raise Exception("Not enough parameters given")

        try:
            self.args = [float(params[0]), float(params[1])]
            self.num_args = 2
        except:
            raise Exception("Wrong argument types")
        
class ArgumentSolver(Argument):

    def __init__(self, *params):
        if len(params) < 1:
            raise Exception("Not enough parameters given")
        
        try:
            self.args = [str(params[0])]
            self.num_args = 1
        except:
            raise Exception("Wrong argument type")
        
class ArgumentContour(Argument):

    def __init__(self, *params):
        if len(params) < 1:
            raise Exception("Not enough parameters given")
        
        try:
            self.args = [str_to_list(params[0], float, True)[0]]
            self.num_args = 1
        except:
            raise Exception("Wrong argument type")

class ProgramState(Enum):
    SD = 0,
    RT = 1,
    AT = 2

def parse(args: list[str]) -> tuple[ProgramState, list[Argument]]:
    i = 1
    res = []

    program_state = None

    if args[0] == "rt" or args[0] == "realtime":
        # rt, realtime [t0] [dt], Plot the graphs in realtime
        program_state = ProgramState.RT

        # Take the second and thrid argument as t0 and dt
        try:
            ar = ArgumentRealtime(*args[1:])
        except Exception as e:
            print(args[0] + ": " + str(e))
            return None
        
        res.append(ar)
        i += 2
    elif args[0] == "sd" or args[0] == "standard":
        # sd, standard, Standard mode of program
        program_state = ProgramState.SD
    elif args[0] == "at" or args[0] == "arnoldtongues":
        # at, arnoldtongues [dt] [min_freq] [max_freq] [step_freq] [min_amp] [max_amp] [step_amp] [N] <entrainment> [diff_equa] <diff_equa_opt>, Generate arnold tongues
        program_state = ProgramState.AT

        if len(args) < 2 or (args[1] != "-i" and args[1] != "--input"):
            # Take the next 10+ arguments as parameters to generate the Arnold tongues from
            try:
                aa = ArgumentArnold(program_state, *args[1:])
            except Exception as e:
                print(args[0] + ": " + str(e))
                return None

            i += aa.num_args
            
            if len(args) <= i:
                print(args[0] + ": Differential equation to solve never given")
                return None
            
            res.append(aa)

            i += 1
            try:
                match args[i-1]:
                    case "h" | "harmonic":
                        # h, harmonic [M] [c] [k], Use forced harmonic oscillator
                        arg = ArgumentHarm(program_state, *args[i:i+3])
                        i += 3
                    case "v" | "vdp" | "vanderpol":
                        # v, vdp, vanderpol [mu], Use forced Van der Pol oscillator
                        arg = ArgumentVan(program_state, *args[i:i+1])
                        i += 1
                    case "vd" | "vilfanduke":
                        # vd, vilfanduke [Kj] [Mj] [Gammaj] [k] [gamma] [B] [left] [right], Use Vilfan Duke oscillator
                        arg = ArgumentVilDuke(program_state, *args[i:i+8])
                        i += 8
                    case _:
                        raise Exception("".join(["Invalid solver ", args[i-1], " given"]))
            except Exception as e:
                print(args[i-1] + ": " + str(e))
                return None

            res.append(arg)
    else:
        # The first argument did not match anything
        return None
    
    # We go through all arguments passed to the function and try to convert them into arguments,
    # containing the argument text, what is needed by the argument and what type it is.
    # To add a new argument, just add a new case, ArgType and add to i, how many extra argument the new argument takes

    while i < len(args):
        try:
            arg = _parse_argument(program_state, args[i:])
        except Exception as e:
            print(args[i] + ": " + str(e))
            return None

        res.append(arg)

        i += arg.num_args + 1
    
    return (program_state, res)

def _parse_argument(program_state, args):
    match args[0]:
        case "-h" | "--help":
            return ArgumentHelp()

        case "-o" | "--out":
            return ArgumentOut(program_state, *args[1:])

        case "-i" | "--input":
            return ArgumentIn(program_state, *args[1:])
        
        case "-e" | "--equation" | "--eq":
            return ArgumentEq(program_state, *args[1:])
        
        case "-d" | "--display":
            return ArgumentDisp(program_state)
        
        case "-s" | "--solver":
            return ArgumentSolver(*args[1:])
        
        case "-c" | "--contour":
            return ArgumentContour(*args[1:])
        
        case "-sh" | "--solveharm":
            return ArgumentHarm(program_state, *args[1:])
        
        case "-sv" | "--solvevandp":
            return ArgumentVan(program_state, *args[1:])
        
        case "-svc" | "--solvevandpcouple":
            return ArgumentVanC(program_state, *args[1:])
        
        case "-svd" | "--solvevilduke":
            return ArgumentVilDuke(program_state, *args[1:])
        
        case "-svdc" | "--solvevildukechain":
            return ArgumentVilDukeC(program_state, *args[1:])
        
        case "-p" | "--progress":
            return ArgumentProg(program_state)
        
        case "-t" | "--threads":
            return ArgumentThread(program_state, *args[1:])
        
        case _:
            raise Exception(args[0] + " is not recognised command")