import struct
import os
from ..graph import Graph, VanDerPolCoupledGraph, VanDerPolForcedGraph, VanDerPolGraph, VilfanDukeGraph, HarmonicGraph
from ..entrainment import ArnoldTongues

def write_file_data(file: str, data: tuple[str, list[float]]):
	# Create directory path if it does not exist
	if not os.path.exists(os.path.dirname(file)):
		os.mkdir(os.path.dirname(file))

	f = open(file, 'wb')

	# Write the version number in the header of the file
	f.write(data[0].encode("utf-8"))

	# Go through the list of float, convert them individually into bytes and write them to the end of the file
	for d in data[1]:
		f.write(struct.pack('d', d))
	
	# Close the file, to flush the contents onto the disk 
	f.close()

def write_file_data_type(file: str, data: tuple[str, list[str | float | int | bytes]]):
	f = open(file, 'wb')

	f.write(data[0].encode("utf-8"))

	for d in data[1]:
		if isinstance(d, str):
			f.write(d.encode("utf-8"))
		elif isinstance(d, int):
			f.write(struct.pack('i', d))
		elif isinstance(d, float):
			f.write(struct.pack('d', d))
		elif isinstance(d, bytes):
			f.write(d)
		else:
			raise TypeError("Type not supported: " + str(type(d)))
		
	f.close()

def convert_data_sets_to_list(sets: list[Graph]) -> list[float]:
	res = []
	
	# Go through all sets and add its data to the resulting list
	for data in sets:
		res.extend(val_list_to_data([float(data.t0), float(data.dt), float(len(data.x))]))
		res.extend(list(map(float, data.x)))
	
	return res

def convert_data_sets_to_list_v11(sets: list[Graph]) -> list[float]:
	res = []
	
	# Go through all sets and add its data to the resulting list
	for data in sets:
		res.extend(val_list_to_data([float(data.t0), float(data.dt)]))
		res.append(len(data.x))
		res.extend(list(map(float, data.x)))
		res.append(len(data.dx))
		res.extend(list(map(float, data.dx)))
	
	return res

def convert_data_sets_to_list_v12(sets: list[Graph]) -> list[float | int | str]:
	res = []

	for graph in sets:
		res.extend(convert_data_set_to_list_v12(graph))
	
	return res

def convert_data_set_to_list_v12(graph: Graph) -> list[float | int | str]:
	res = []

	res.append(len(graph.name))
	res.append(graph.name)
	if isinstance(graph, VanDerPolForcedGraph):
		res.extend(val_list_to_data([b'f', float(graph.t0), float(graph.dt), float(graph.mu), graph.F, list(map(float, graph.x)), list(map(float, graph.dx))]))
	elif isinstance(graph, VanDerPolCoupledGraph):
		res.extend(val_list_to_data([b'c', float(graph.t0), float(graph.dt), float(graph.mu), float(graph.k), list(map(float, graph.x)), list(map(float, graph.dx))]))
	elif isinstance(graph, VanDerPolGraph):
		res.extend(val_list_to_data([b'p', float(graph.t0), float(graph.dt), float(graph.mu), list(map(float, graph.x)), list(map(float, graph.dx))]))
	elif isinstance(graph, VilfanDukeGraph):
		res.extend(val_list_to_data([b'v', float(graph.t0), float(graph.dt), float(graph.omegaj), float(graph.epsilonj), float(graph.dr), float(graph.di), float(graph.B), graph.zeta, graph.left, graph.right, list(map(float, graph.x)), list(map(float, graph.dx))]))
	elif isinstance(graph, HarmonicGraph):
		res.extend(val_list_to_data([b'h', float(graph.t0), float(graph.dt), float(graph.m), float(graph.c), float(graph.k), list(map(float, graph.x)), list(map(float, graph.dx))]))
	else:
		res.extend(val_list_to_data([b'g', float(graph.t0), float(graph.dt), list(map(float, graph.x)), list(map(float, graph.dx))]))

	return res

def convert_arnold_tongue_to_list(at: ArnoldTongues) -> list[float | int | str]:
	res = []

	res.extend(val_list_to_data([float(at.f_min), float(at.f_max), float(at.df), float(at.a_min), float(at.a_max), float(at.da), float(at.omega_0)]))
	res.extend(val_list_to_data([list(map(lambda l : list(map(float, l)), at.windings))]))
	res.append(len(at.points))
	for p in at.points:
		res.append(len(p))
		res.extend(convert_data_sets_to_list_v12(p))

	return res

def convert_arnold_tongue_to_list_v14(at: ArnoldTongues) -> list[float | int | str]:
	res = []

	res.extend(val_list_to_data([float(at.f_min), float(at.f_max), float(at.df), float(at.a_min), float(at.a_max), float(at.da), float(at.omega_0)]))
	res.extend(val_list_to_data([list(map(lambda l : list(map(float, l)), at.windings))]))
	res.extend(convert_data_set_to_list_v12(at.points[0][0]))

	return res

def val_list_to_data(vals: list[any]):
	res = []

	for v in vals:
		if isinstance(v, list):
			res.append(len(v))
			res.extend(val_list_to_data(v))
		elif isinstance(v, str):
			res.append(len(v))
			res.append(v)
		else:
			res.append(v)
	
	return res

def write_data_sets_v1(file: str, sets: list[Graph]):
	# First we convert the graphs to a serialized float array
	data = convert_data_sets_to_list(sets)

	# Then we write that float array to the disk, with the version number "1.0" as the header 
	write_file_data(file, ("1.0", data))

def write_data_sets_v11(file: str, sets: list[Graph]):
	# First we convert the graphs to a serialized float array
	data = convert_data_sets_to_list_v11(sets)

	# Then we write that float array to the disk, with the version number "1.1" as the header 
	write_file_data(file, ("1.1", data))

def write_data_sets_v12(file: str, sets: list[Graph]):
	# First we convert the graphs to a serialized float array
	data = convert_data_sets_to_list_v12(sets)

	# Then we write that float array to the disk, with the version number "1.2" as the header 
	write_file_data_type(file, ("1.2", data))

def write_data_sets_v13(file: str, sets: list[Graph | ArnoldTongues]):
	# First we convert the graphs and Arnold tongues to a serialized data array
	data = []
	for s in sets:
		if isinstance(s, ArnoldTongues):
			data.append('at')
			data.extend(convert_arnold_tongue_to_list(s))
		else:
			data.append('sd')
			data.extend(convert_data_set_to_list_v12(s))

	# Then we write that data array to the disk, with the version number "1.3" as the header 
	write_file_data_type(file, ("1.3", data))


def write_data_sets_v14(file: str, sets: list[Graph | ArnoldTongues]):
	# First we convert the graphs and Arnold tongues to a serialized data array
	data = []
	for s in sets:
		if isinstance(s, ArnoldTongues):
			data.append('at')
			data.extend(convert_arnold_tongue_to_list_v14(s))
		else:
			data.append('sd')
			data.extend(convert_data_set_to_list_v12(s))

	# Then we write that data array to the disk, with the version number "1.3" as the header 
	write_file_data_type(file, ("1.4", data))
