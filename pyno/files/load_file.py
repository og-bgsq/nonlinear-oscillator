import struct
from ..graph import Graph, VanDerPolCoupledGraph, VanDerPolForcedGraph, VanDerPolGraph, VilfanDukeGraph, HarmonicGraph
from ..entrainment import ArnoldTongues

def load_file_data(file: str) -> list[tuple[str, bytes]]:
	f = open(file, 'rb')

	# The first three bytes of a file, is always the version code, this is used to convert the data into a usable graph
	ver = f.read(3).decode("utf-8")

	# The rest of the data is the actual file, this is stored as bytes, so we can convert it later to whatever is needed
	res = f.read()

	f.close()

	return (ver, res)

def bytes_to_float_array(data: bytes, N: int) -> list[float]:
	# Convert 8 * N bytes of data, into a float array, by unpacking the data one float at a time

	res = []
	for i in range(N):
		res.append(struct.unpack('d', data[i * 8:(i + 1) * 8])[0])
	
	return res

def parse_file_data_v1(data: bytes) -> list[Graph]:
	i = 0
	res = []

	# Go through all the data and extract t0, dt, N and a float array, then append it to the graph result list and continue until all the data has been parsed
	while i < len(data):
		(t0, dt, n), di = unpack(data[i:], 'fff')

		i += di

		y = bytes_to_float_array(data[i:], int(n))
		i += int(n) * 8

		res.append(Graph(name = "", t0 = t0, dt = dt, x = y))
		
	return res

def parse_file_data_v11(data: bytes) -> list[Graph]:
	i = 0
	res = []

	# Go through all the data and extract t0, dt, N and a float array, then append it to the graph result list and continue until all the data has been parsed
	while i < len(data):
		(t0, dt, ny), di = unpack(data[i:], 'fff')

		i += di

		y = bytes_to_float_array(data[i:], int(ny))
		i += int(ny) * 8

		(ndy,), di = unpack(data[i:], 'f')

		i += di

		dy = bytes_to_float_array(data[i:], int(ndy))
		i += int(ndy) * 8

		res.append(Graph(name = "", t0 = t0, dt = dt, x = y, dx = dy))
		
	return res

def parse_file_data_v12(data: bytes) -> list[VanDerPolForcedGraph | VanDerPolCoupledGraph | VanDerPolGraph | VilfanDukeGraph | HarmonicGraph | Graph]:
	i = 0
	res = []

	while i < len(data):
		r, di = parse_next_v12(data)

		if r:
			res.append(r)
		i += di

	return res

def parse_next_v12(data: bytes) -> VanDerPolForcedGraph | VanDerPolCoupledGraph | VanDerPolGraph | VilfanDukeGraph | HarmonicGraph | Graph | None:
	(name, graph_type, t0, dt), i = unpack(data, 'sbff')

	match graph_type:
		case b'f':
			(mu, F, y, dy), di = unpack(data[i:], 'fsll')
			return (VanDerPolForcedGraph(name, t0, dt, y, dy, mu, F), i + di)
		case b'c':
			(mu, k, y, dy), di = unpack(data[i:], 'ffll')
			return (VanDerPolCoupledGraph(name, t0, dt, y, dy, mu, k), i + di)
		case b'p':
			(mu, y, dy), di = unpack(data[i:], 'fll')
			return (VanDerPolGraph(name, t0, dt, y, dy, mu), i + di)
		case b'v':
			(omegaj, epsilonj, dr, di, B, zeta, left, right, y, dy), dind = unpack(data[i:], 'fffffsssll')
			return (VilfanDukeGraph(name, t0, dt, y, dy, omegaj, epsilonj, dr, di, B, zeta, left, right), i + dind)
		case b'h':
			(m, c, k, y, dy), di = unpack(data[i:], 'fffll')
			return (HarmonicGraph(name, t0, dt, y, dy, m, c, k), i + di)
		case b'g':
			(y, dy), di = unpack(data[i:], 'll')
			return (Graph(name, t0, dt, y, dy), i + di)

def parse_file_data_v13(data: bytes) -> list[VanDerPolForcedGraph | VanDerPolCoupledGraph | VanDerPolGraph | VilfanDukeGraph | HarmonicGraph | Graph | ArnoldTongues]:
	i = 0
	res = []

	res_append = res.append

	while i < len(data):
		match data[i:i+2]:
			case b'sd':
				r, di = parse_next_v12(data[i+2:])
				
				res_append(r)
				i += di + 2
			case b'at':
				(f_min, f_max, df, a_min, a_max, da, omega_0, w), di = unpack(data[i+2:], 'fffffffL')
				i += di + 2

				# Go through all graphs, describing each point on the Arnold tongue and add them to the rows list and add all rows to the ps (points) list
				(n,), di = unpack(data[i:], 'i') # Get number of rows
				i += di

				ps = []
				ps_append = ps.append
				for _ in range(n):
					(m,), di = unpack(data[i:], 'i') # Get number of columns on this row
					i += di

					row = []
					row_append = row.append
					for _ in range(m):
						g, di = parse_next_v12(data[i:]) # Get the graph, that describes this point and add it to the row list
						i += di

						row_append(g)
					
					ps_append(row)

				res_append(ArnoldTongues(w, ps, a_max, a_min, da, f_max, f_min, df, omega_0))

	return res

def parse_file_data_v14(data: bytes) -> list[VanDerPolForcedGraph | VanDerPolCoupledGraph | VanDerPolGraph | VilfanDukeGraph | HarmonicGraph | Graph | ArnoldTongues]:
	i = 0
	res = []

	res_append = res.append

	while i < len(data):
		match data[i:i+2]:
			case b'sd':
				r, di = parse_next_v12(data[i+2:])
				
				res_append(r)
				i += di + 2
			case b'at':
				(f_min, f_max, df, a_min, a_max, da, omega_0, w), di = unpack(data[i+2:], 'fffffffL')
				i += di + 2

				g, di = parse_next_v12(data[i:])
				i += di

				res_append(ArnoldTongues(w, [[g]], a_max, a_min, da, f_max, f_min, df, omega_0))

	return res

def unpack(d: bytes, types: str) -> tuple[list[any], int]:
	res = []
	i = 0

	res_append = res.append
	struct_unpack = struct.unpack

	for t in types:
		match t:
			case 'b':
				res_append(d[i:i+1])
				i += 1
			case 'i':
				res_append(struct_unpack('i', d[i:i+4])[0])
				i += 4
			case 'f':
				res_append(struct_unpack('d', d[i:i+8])[0])
				i += 8
			case 's':
				l, = struct_unpack('i', d[i:i+4])
				res_append(d[i+4:i+4+l].decode("utf-8"))
				i += 4 + l
			case 'l':
				n, = struct_unpack('i', d[i:i+4])
				res_append(bytes_to_float_array(d[i+4:], n))
				i += 4 + n * 8
			case 'L':
				n, = struct_unpack('i', d[i:i+4])
				r, di = unpack(d[i+4:], 'l' * n)
				res_append(r)
				i += di + 4

	return (res, i)
		
def load_data_sets(file: str) -> list[VanDerPolForcedGraph | VanDerPolCoupledGraph | VanDerPolGraph | VilfanDukeGraph | HarmonicGraph | Graph | ArnoldTongues]:
	# Load the dataset from file
	data = load_file_data(file)

	# Use the specific parting function to parse the data, based on the version number from the file
	if data[0] == "1.0":
		return parse_file_data_v1(data[1])
	elif data[0] == "1.1":
		return parse_file_data_v11(data[1])
	elif data[0] == "1.2":
		return parse_file_data_v12(data[1])
	elif data[0] == "1.3":
		return parse_file_data_v13(data[1])
	elif data[0] == "1.4":
		return parse_file_data_v14(data[1])
