import numpy as np
import time
from multiprocessing import Process, active_children, Manager
from types import FunctionType

from ..graph import Graph
from .entrainment import get_winding_number_spr, get_last_peak

from .entrainment import get_winding_number_rev, get_winding_numbers

class ArnoldTongues:
    windings: list[list[float]]
    points: list[list[Graph]]

    a_max: float
    a_min: float
    da: float

    f_max: float
    f_min: float
    df: float

    omega_0: float

    def __init__(self, windings: list[list[float]], points: list[list[Graph]], a_max: float, a_min: float, da: float, f_max: float, f_min: float, df: float, omega_0: float):
        self.windings = windings
        self.points = points

        self.a_max = a_max
        self.a_min = a_min
        self.da = da

        self.f_max = f_max
        self.f_min = f_min
        self.df = df

        self.omega_0 = omega_0
    
    def get_windings(self, freq, amp):
        if freq < self.f_min or freq > self.f_max or amp < self.a_min or amp > self.a_max:
            return None

        sf = (freq - self.f_min) // self.df
        sa = (amp - self.a_min) // self.da

        return self.windings[sa][sf]

def get_arnold_tongues(f1: FunctionType, min_amp: float, max_amp: float, step_amp: float, min_freq: float, max_freq: float, step_freq: float, dt: float, omega_0: float, progress: bool = False, num_threads: int = 1):
    if num_threads == 1:
        # Use old, non multithreaded, function
        return _get_arnold_tongues(f1, min_amp, max_amp, step_amp, min_freq, max_freq, step_freq, dt, omega_0, progress)
    else:
        # Use new multithreaded function
        return _get_arnold_tongues_multi(f1, min_amp, max_amp, step_amp, min_freq, max_freq, step_freq, dt, omega_0, progress, num_threads)

def _get_arnold_tongues_multi(f1: FunctionType, min_amp: float, max_amp: float, step_amp: float, min_freq: float, max_freq: float, step_freq: float, dt: float, omega_0: float, progress: bool, num_threads: int):
    NUM_REV = 15

    # The main thread also counts, so add one to get number of extra threads
    num_threads += 1

    def winding_number_thread(amp_i, windings, points, f1):
        freq_w = []
        freq_p = []

        for freq in np.arange(min_freq, max_freq, step_freq):
            y1, dy1, g1 = f1(freq, amp)

            peak = get_last_peak(y1)

            freq_w.append(get_winding_number_spr(list(zip(y1[:peak], dy1[:peak])), NUM_REV, dt, freq))

            freq_p.append(g1)
        
        windings[amp_i] = freq_w
        points[amp_i] = freq_p

    if progress:
        print()
    
    amp_range = np.arange(min_amp, max_amp, step_amp)

    manager = Manager()

    windings = manager.list([None for _ in amp_range])
    points = manager.list([None for _ in amp_range])

    for amp_i, amp in enumerate(amp_range):
        while len(active_children()) >= num_threads: time.sleep(1)

        if progress:
            print("Generating Arnold tongues: " + ('{0:.2f}'.format((amp - min_amp) / (max_amp - min_amp) * 100)) + "%", end="\r")

        Process(target=winding_number_thread, args=(amp_i, windings, points, f1)).start()
        
    while len(active_children()) > 1: pass
        
    if progress:
        print()

    return ArnoldTongues(windings, points, max_amp, min_amp, step_amp, max_freq, min_freq, step_freq, omega_0)

def _get_arnold_tongues(f1: FunctionType, min_amp: float, max_amp: float, step_amp: float, min_freq: float, max_freq: float, step_freq: float, dt: float, omega_0: float, progress: bool):
    NUM_REV = 15

    if progress:
        print()

    windings = []
    points = []
    for amp in np.arange(min_amp, max_amp, step_amp):
        freq_w = []
        freq_p = []

        if progress:
            print("Generating Arnold tongues: " + ('{0:.2f}'.format((amp - min_amp) / (max_amp - min_amp) * 100)) + "%", end="\r")

        for freq in np.arange(min_freq, max_freq, step_freq):
            y1, dy1, g1 = f1(freq, amp)

            peak = get_last_peak(y1)

            freq_w.append(get_winding_number_spr(list(zip(y1[:peak], dy1[:peak])), NUM_REV, dt, freq))

            freq_p.append(g1)
        
        windings.append(freq_w)
        points.append(freq_p)
        
    if progress:
        print()
    
    return ArnoldTongues(windings, points, max_amp, min_amp, step_amp, max_freq, min_freq, step_freq, omega_0)