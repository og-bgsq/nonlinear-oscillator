from scipy.signal import hilbert
import numpy
import math
from itertools import zip_longest

def is_entrained_hilbert(s1: list[float], s2: list[float]):
    EPSILON = 0.1

    y1 = hilbert(s1)
    y2 = hilbert(s2)

    first_angle = numpy.angle(y1[0]) - numpy.angle(y2[0])

    for (e1, e2) in zip(y1[:-1], y2[:-1]):
        a1 = numpy.angle(e1)
        a2 = numpy.angle(e2)

        angle = a1 - a2

        if _min_angle_diff(angle, first_angle) > EPSILON:
            return False

    return True

def is_entrained_winding_numbers(s1: list[tuple[float, float]], s2: list[tuple[float, float]]):
    EPSILON = 0.95
    
    nm = get_winding_numbers(s1, s2)

    return nm > EPSILON and nm < 1 / EPSILON

def get_winding_numbers(s1: list[tuple[float, float]], s2: list[tuple[float, float]]):
    n1 = _get_winding_number(s1)
    n2 = _get_winding_number(s2)

    if n2 == 0:
        return math.inf

    return n1 / n2

def get_winding_number_rev(s1: list[tuple[float, float]], num_rev: float) -> float:
    n1 = _get_winding_number(s1)

    return abs(n1 / num_rev)

def get_winding_number_spr(s1: list[tuple[float, float]], num_rev: float, dt: float, omega: float) -> float:
    num = get_samples_per_rev_energy(s1)

    if num == 0:
        return math.inf

    return abs((2 * math.pi / (omega * dt)) / num)

def get_last_zero_crossing(s: list[float]):
    prev_a = 0
    for i, a in reversed(list(enumerate(s))):
        # If current and previus point multiplied is negative, then they have opposite signs and is thus a zero crossing
        if a * prev_a < 0:
            return i

        prev_a = a

def get_last_peak(s: list[float]):
    best = None
    prev = prev_prev = None

    for i, a in reversed(list(enumerate(s))):
        if prev_prev:
            if (prev_prev < prev and a < prev) or (prev_prev > prev and a > prev):
               best = i + 1

            if best and ((a < 0 and prev > 0) or (a > 0 and prev < 0)):
                return best

        prev_prev = prev
        prev = a

def get_entrainment_times_wn(s1: list[tuple[float, float]], s2: list[tuple[float, float]], window_size: int = 100):
    entrained = []

    for i in range(len(s1)):
        entrained.append(is_entrained_winding_numbers(s1[i:i+window_size], s2[i:i+window_size]))

    return entrained

def is_entrained_with_period(s: list[float], omega: float, dt: float):
    return _has_N_period(s, int(omega / dt))

def get_samples_per_rev_energy(s: list[tuple[float, float]]) -> float:
    MIN_MAG = 0.001
    NUM_MIN_MAG_EXIT = 100

    best = prev_best = None

    first_point = None
    prev_cross = 0

    samples = revolutions = 0

    num_min_mag = 0

    for (amp, vel) in s:

        point = (amp, vel)

        if amp * amp + vel * vel < MIN_MAG:
            num_min_mag += 1

            if num_min_mag > NUM_MIN_MAG_EXIT:
                return prev_best
        else:
            num_min_mag = 0

        if first_point is None:
            first_point = point
            continue

        cross = first_point[0] * point[1] - first_point[1] * point[0]

        if first_point[0] * point[0] + first_point[1] * point[1] > 0:
            if cross < 0 and prev_cross > 0:
                revolutions -= 1

                if revolutions < 0:
                    prev_best = best
                    best = samples / revolutions
            elif cross > 0 and prev_cross < 0:
                revolutions += 1
                
                if revolutions > 0:
                    prev_best = best
                    best = samples / revolutions

        samples += 1

        prev_cross = cross
    return prev_best

def _get_samples_per_revolutions(s: list[tuple[float, float]], num_rev: int) -> float:
    first_point = None
    prev_cross = 0

    samples = revolutions = 0

    best = None

    for (amp, vel) in reversed(s):
        point = (amp, vel)

        if not first_point:
            first_point = point
            samples += 1
            continue

        cross = first_point[0] * point[1] - first_point[1] * point[0]

        if first_point[0] * point[0] + first_point[1] * point[1] > 0:
            if cross < 0 and prev_cross > 0:
                revolutions -= 1

                if revolutions == -num_rev:
                    best = samples / num_rev
                elif revolutions == -num_rev - 1:
                    return best
            elif cross > 0 and prev_cross < 0:
                revolutions += 1

                if revolutions == num_rev:
                    best = samples / num_rev
                elif revolutions == num_rev + 1:
                    return best

        samples += 1

        prev_cross = cross
    
    return 0
    #raise Exception("Not enough samples to determine points for " + str(num_rev) + " revolutions")

def _get_winding_number(s: list[tuple[float, float]]) -> float:
    angle_sum = 0

    prev_ang = None

    for (amp, vel) in s:
        ang = _get_angle_from_amp_vel(amp, vel)

        if prev_ang:
            angle_sum += _min_angle_diff(ang, prev_ang)

        prev_ang = ang

    return angle_sum / (2 * math.pi)

def _get_angle_from_amp_vel(amp: float, vel: float) -> float:
    if amp == 0:
        ang = math.pi / 2.0 if vel > 0 else -math.pi / 2.0
    else:
        ang = math.atan(vel / amp)
    
    if amp < 0:
        ang += math.pi

    if ang < 0:
        ang += 2 * math.pi
    
    return ang

def _has_N_period(xr: list[float], N: int):
    EPSILON = 0.01

    N = abs(N)

    y = hilbert(xr)

    s = 0
    for i in reversed(range(0, len(y), N)):
        s += y[i] / abs(y[i])
    
        sqmag = s.real ** 2 + s.imag ** 2

        if sqmag < EPSILON:
            return False

    return True

def _angle_diff(a1: float, a2: float, absolute = True) -> bool:
    while abs(a1 - a2) > math.pi:
        if a1 < a2:
            a2 -= math.pi
        else:
            a1 -= math.pi

    diff = a1 - a2

    if absolute:
        diff = abs(diff)

    return diff

def _min_angle_diff(a1: float, a2: float) -> float:
    diff = a1 - a2

    while diff < -math.pi:
        diff += 2 * math.pi

    while diff > math.pi:
        diff -= 2 * math.pi
    
    return diff