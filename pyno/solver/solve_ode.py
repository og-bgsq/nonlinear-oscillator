import numpy as np
from scipy.integrate import odeint, complex_ode, ode
from types import FunctionType
from .damped_harmonic_oscillator import damped_harmonic_oscillator, damped_harmonic_forced_oscillator
from .van_der_pol_oscillator import van_der_pol_oscillator, van_der_pol_coupled_oscillator, van_der_pol_forced_oscillator, van_der_pol_fa_forced_oscillator
from .vilfan_duke_oscillator import vilfan_duke_oscillator

solver = "vode"

def solve(func, init_cond, t0, dt, N, params = []):
    r = ode(func)
    r.set_initial_value(init_cond, t0)
    r.set_f_params(params)
    if not " " in solver:
        r.set_integrator(solver)
    else:
        s, m = solver.split(" ")
        r.set_integrator(s, method = m)
    res = [list(r.integrate(r.t+dt)) for _ in range(N - 1)]
    init = [[e for e in init_cond]]
    init.extend(res)
    return list(zip(*init))

def solve_complex(func, t0, dt, N, y0, params = []):
    def helper(t, y):
        return func(y, t, params)

    r = complex_ode(helper)
    r.set_integrator(solver)
    r.set_initial_value(y0, t0)
    res = [r.integrate(r.t+dt)[0] for _ in range(N - 1)]
    init = [y0]
    init.extend(res)
    return init

def solve_damped_harmonic(t0, dt, N, y0, dy0, M, c, k):
    return solve(damped_harmonic_oscillator, [y0, dy0], t0, dt, N, [M, c, k])

def solve_forced_damped_harmonic(t0, dt, N, y0, dy0, M, c, k, F):
    return solve(damped_harmonic_forced_oscillator, [y0, dy0], t0, dt, N, [M, c, k, F])

def solve_van_der_pol(t0, dt, N, y0, dy0, mu):
    return solve(van_der_pol_oscillator, [y0, dy0], t0, dt, N, [mu])

def solve_forced_van_der_pol(t0, dt, N, y0, dy0, mu, F):
    return solve(van_der_pol_forced_oscillator, [y0, dy0], t0, dt, N, [mu, F])

def solve_fa_forced_van_der_pol(t0, dt, N, y0, dy0, mu, a, f):
    return solve(van_der_pol_fa_forced_oscillator, [y0, dy0], t0, dt, N, [mu, a, f])

def solve_van_der_pol(t0, dt, N, y0, dy0, mu):
    return solve(van_der_pol_oscillator, [y0, dy0], t0, dt, N, [mu])

def solve_vilfan_duke(t0, dt, N, y0, Kj, Mj, Gammaj, k, gamma, zeta, B, left, right):
    sol = solve_complex(vilfan_duke_oscillator, t0, dt, N, y0, [Kj, Mj, Gammaj, k, gamma, zeta, B, left, right])
    
    y = []
    dy = []

    for c in sol:
        y.append(c.real)
        dy.append(c.imag)

    return (y, dy)

def solve_vilfan_duke_chain(t0, dt, N, M, y0, Kj, Mj, Gammaj, k, gamma, zeta, B):
    # TODO: solve everything in one go (Van der Pol), this should be much faster
    def c(c):
        return lambda _ : c
    
    rs = [get_complex_ode(vilfan_duke_oscillator, t0, y0[i], [Kj[i], Mj[i], Gammaj[i], k[i], gamma[i], zeta[i], B[i], c(0), c(0)]) for i in range(M)]
    
    res = [[] for _ in range(M)]

    t = t0

    for _ in range(N):
        for i in range(M):
            real, imag = get_next_val(rs[i], dt)
            res[i].append(real+imag*1j)

        t += dt
        for i in range(1, M - 1):
            rs[i] = get_complex_ode(vilfan_duke_oscillator, t, res[i][-1], [Kj[i], Mj[i], Gammaj[i], k[i], gamma[i], zeta[i], B[i], c(res[i - 1][-1]), c(res[i + 1][-1])])
        rs[0] = get_complex_ode(vilfan_duke_oscillator, t, res[0][-1], [Kj[0], Mj[0], Gammaj[0], k[0], gamma[0], zeta[0], B[0], c(0), c(res[0 + 1][-1])])
        rs[-1] = get_complex_ode(vilfan_duke_oscillator, t, res[i][-1], [Kj[-1], Mj[-1], Gammaj[-1], k[-1], gamma[-1], zeta[-1], B[-1], c(res[-1 - 1][-1]), c(0)])

    return res

def solve_van_der_pol_coupled(t0, dt, N, y0, dy0, mu, k, n):
    def helper(y, t, params):
        return van_der_pol_coupled_oscillator(y, t, params, n)
    
    return solve(helper, [e for t in zip(y0, dy0) for e in t], t0, dt, N, [mu, k])


def get_ode(func: FunctionType, t0: float, y0: float, dy0: float, params: list[float | FunctionType] = []) -> ode:
    r = ode(func)
    r.set_integrator(solver)
    r.set_initial_value([y0, dy0], t0).set_f_params(params)
    return r

def get_complex_ode(func: FunctionType, t0: float, y0: complex, params: list[complex | FunctionType] = []) -> complex_ode:
    def helper(t, y):
        return func(y, t, params)

    r = complex_ode(helper)
    r.set_integrator(solver)
    r.set_initial_value(y0, t0)
    return r

def get_next_val(r: ode | complex_ode, dt: float) -> float | complex:
    res = r.integrate(r.t + dt)

    if isinstance(r, complex_ode):
        res = (res.real, res.imag)
    
    return res

def get_damped_harmonic_rt(t0: float, y0: float, dy0: float, M: float, c: float, k: float) -> complex_ode:
    r = get_ode(damped_harmonic_oscillator, t0, y0, dy0, [M, c, k])
    def helper(dt):
        #get_next_val(r, dt)
        return get_next_val(r, dt)

    return helper

def get_forced_damped_harmonic_rt(t0: float, y0: float, dy0: float, M: float, c: float, k: float, F: FunctionType) -> complex_ode:
    r = get_ode(damped_harmonic_forced_oscillator, t0, y0, dy0, [M, c, k, F])
    def helper(dt):
        #get_next_val(r, dt)
        return get_next_val(r, dt)

    return helper

def get_van_der_pol_rt(t0: float, y0: float, dy0: float, mu) -> complex_ode:
    r = get_ode(van_der_pol_oscillator, t0, y0, dy0, [mu])
    def helper(dt):
        return get_next_val(r, dt)
    
    return helper

def get_forced_van_der_pol_rt(t0: float, y0: float, dy0: float, mu: float, F: FunctionType) -> complex_ode:
    r = get_ode(van_der_pol_forced_oscillator, t0, y0, dy0, [mu, F])
    def helper(dt):
        return get_next_val(r, dt)
    
    return helper

def get_vilfan_duke_rt(t0: float, y0: complex, Kj, Mj, Gammaj, k, gamma, zeta, B, left, right) -> complex_ode:
    r = get_complex_ode(vilfan_duke_oscillator, t0, y0, [Kj, Mj, Gammaj, k, gamma, zeta, B, left, right])
    def helper(dt):
        return get_next_val(r, dt)
    
    return helper

def list_to_force(l: list[float], t0: float, dt: float, default: float = 0) -> FunctionType:
    def f(t):
        i = round((t - t0) / dt)

        if i < 0 or i >= len(l):
            return default

        return l[i]

    return f
