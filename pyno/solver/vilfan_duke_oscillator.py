import numpy as np

def vilfan_duke_oscillator(y, t, params):
    zj = y
    Kj, Mj, Gammaj, k, gamma, zeta, B, left, right = params
    omegaj = np.sqrt(Kj/Mj)
    epsilonj = -(Gammaj)/(2*Mj)
    dI = -(1/2)*(k/(np.sqrt(Kj*Mj)))
    dR = (1/2)*(gamma/Mj)
    sf = zeta(t)/(Mj*omegaj)
    if not left and not right:
        cc = 0
    elif not left:
        cc = right(t)-zj
    elif not right:
        cc = left(t)-zj
    else:
        cc = left(t)+right(t)-2*zj
    deriv = (omegaj*1j+epsilonj)*zj+(dR+dI*1j)*cc+sf-(B*(np.abs(zj)**2)*zj)
    return [deriv]