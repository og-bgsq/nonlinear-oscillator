import numpy as np

def damped_harmonic_oscillator(_, y, params):
    x, omega = y #omega equals the derivative of x with respect to time
    m, c, k = params #m is the mass being pulled by the force, c is the damping constant and k is the spring constant of the oscillator
    derivs = [omega, (-k*x-c*omega)/m] #returns the derivative of x with respect to time, and of dx with respect to time
    return derivs

def damped_harmonic_forced_oscillator(t, y, params):
    x, omega = y
    m, k, c, F = params
    derivs = [omega, (-k*x-c*omega)/m+F(t)]
    return derivs

