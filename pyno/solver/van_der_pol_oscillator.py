import math

def van_der_pol_oscillator(t, y, params):
    x, omega = y #omega equals the derivative of x with respect to time
    mu, = params #mu describes the nonlinearity of the system
    derivs = [omega, mu*(1-x**2)*omega-x] #returns dx and the derivative of dx
    return derivs

def van_der_pol_forced_oscillator(t, y, params):
    x, omega = y
    mu, F = params
    derivs = [omega, mu*(1-x**2)*omega-x+F(t)]
    return derivs

def van_der_pol_fa_forced_oscillator(t, y, params):
    x, omega = y
    mu, a, f = params
    derivs = [omega, mu*(1-x**2)*omega-x+a*math.sin(f*t)]
    return derivs

# x1, omega1, x2, omega2, ..., xn, omegan

def van_der_pol_coupled_oscillator(t, y, params, n):
    mu, k = params

    derivs = [y[1], mu[0]*(1-y[0]**2)*y[1]-y[0]+k[0]*(y[2]-y[0])]

    for i in range(1, n-1):
        xi, omegai = y[i*2:i*2+2]
        derivs.extend([omegai, mu[i]*(1-xi**2)*omegai-xi+k[i]*(y[(i-1)*2]+y[(i+1)*2]-2*xi)])
    
    derivs.extend([y[-1], mu[-1]*(1-y[-2]**2)*y[-1]-y[-2]+k[-1]*(y[-4]-y[-2])])

    return derivs