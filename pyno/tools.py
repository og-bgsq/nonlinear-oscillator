import math
from types import FunctionType
from .entrainment import get_samples_per_rev_energy

def parse_stdin(input: str | None) -> list[list[float]]:
    if not input:
        return []

    res = None
    if input.startswith("["):
        # Input is a list
        res = str_to_list(input)

    return res

def str_to_list(string: str, conv_func: FunctionType = float, should_fail_on_error: bool = False) -> list[list]:
    if should_fail_on_error:
        if not string.startswith('[') or not string.endswith(']'):
            raise Exception("Input string does not contain start or end bracket")
    
    res = []
    l = []
    curr = ""
    # Go through all characters of input
    for c in string:
        if c == '[': continue

        # If we hit a ',' or ']', then we convert what we already have to a float, set the current value to ""
        if c == ',' or c == ']':
            try:
                l.append(conv_func(curr))
            except Exception as e:
                if should_fail_on_error:
                    raise e
                pass
            curr = ""

            # if character is ']', then we add the list to res
            if c == ']':
                res.append(l)
                l = []
            continue

        # Else we add the current character
        curr += c

    return res

def calc_ang_vel(s: list[tuple[float, float]], dt: float) -> float:
    num = get_samples_per_rev_energy(s)

    return 2 * math.pi / (num * dt)