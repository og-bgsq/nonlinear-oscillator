from .graph_generator import graph_from_equation, create_function, create_fa_function
from .graph import *
