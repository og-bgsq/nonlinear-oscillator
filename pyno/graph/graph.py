import math

class Graph:
    t0: float
    dt: float
    x: list[float]
    dx: list[float]
    name: str
    
    def __init__(self, name, t0, dt, x, dx = []):
        self.t0 = t0
        self.dt = dt
        self.x = x
        self.dx = dx
        self.name = name

    def calc_dx(self):
        # Calculate the difference of x vector, by subtracting elements of the x vector and dividing it with dt
        
        self.dx = []
        
        for i in range(len(self.x) - 1):
            self.dx.append((self.x[i + 1] - self.x[i]) / self.dt)

    def __repr__(self):
        return "Graph(t0 = " + str(self.t0) + ", dt = " + str(self.dt) + ", x = " + str(self.x) + ", dx = " + str(self.dx) + ")"
    
    def __str__(self):
        return self.name
    
    def __eq__(self, other):
        return self.t0 == other.t0 and self.dt == other.dt and self.x == other.x and self.dx == other.dx and self.name == other.name

class HarmonicGraph(Graph):
    m: float
    c: float
    k: float

    def __init__(self, name, t0, dt, x, dx, m, c, k):
        super().__init__(name, t0, dt, x, dx)
        self.m = m
        self.c = c
        self.k = k

    def __str__(self):
        acc = str(self.m)+"d^2y/dt^2"
        vel = str(-self.c)+"dy/dt"
        if self.k < 0:
            pos = "+"+str(-self.k)+"y"
        else:
            pos = str(-self.k)+"y"
        return acc+"="+vel+pos
    
    def __eq__(self, other):
        return super().__eq__(other) and self.m == other.m and self.c == other.c and self.k == other.k

class VanDerPolGraph(Graph):
    mu: float

    def __init__(self, name, t0, dt, x, dx, mu):
        super().__init__(name, t0, dt, x, dx)
        self.mu = mu

    def __str__(self):
        acc = "d^2y/dt^2"
        vel = str(self.mu)+"(1-y^2)dy/dt"
        pos = "-y"
        return acc+"="+vel+pos
    
    def __eq__(self, other):
        return super().__eq__(other) and self.mu == other.mu

class VanDerPolForcedGraph(VanDerPolGraph):
    F: str

    def __init__(self, name, t0, dt, x, dx, mu, F):
        super().__init__(name, t0, dt, x, dx, mu)
        self.F = F

    def __str__(self):
        if self.F.startswith("-"):
            return super().__str__() + self.F
            
        return super().__str__() + "+" + self.F
    
    def __eq__(self, other):
        return super().__eq__(other) and self.F == other.F

class VanDerPolCoupledGraph(VanDerPolGraph):
    k: float

    def __init__(self, name, t0, dt, x, dx, mu, k):
        super().__init__(name, t0, dt, x, dx, mu)
        self.k = k

    def __str__(self):
        if self.k < 0:
            return super().__str__() + str(self.k)
        return super().__str__() + "+" + str(self.k)
    
    def __eq__(self, other):
        return super().__eq__(other) and self.k == other.k

class VilfanDukeGraph(Graph):
    omegaj: float
    epsilonj: float
    dr: float
    di: float
    B: float
    zeta: str
    left: str
    right: str

    def __init__(self, *params):
        if len(params) == 13:
            self._sd_init(*params)
        elif len(params) == 12:
            self._cal_init(*params)
        elif len(params) == 14:
            self._cal_init(*params)
        else:
            raise Exception("Not enough arguments to create Vilfan and Duke Graph")

    def _sd_init(self, name, t0, dt, x, dx, omegaj, epsilonj, dr, di, B, zeta, left, right):
        super().__init__(name, t0, dt, x, dx)
        self.omegaj = omegaj
        self.epsilonj = epsilonj
        self.dr = dr
        self.di = di
        self.B = B
        self.zeta = zeta
        self.left = left
        self.right = right

    def _cal_init(self, name, t0, dt, x, dx, Kj, Mj, Gammaj, k, gamma, zeta, B, left = "l", right = "r"):
        omegaj = math.sqrt(Kj/Mj)
        epsilonj = -(Gammaj)/(2*Mj)
        di = -(1/2)*(k/(math.sqrt(Kj*Mj)))
        dr = (1/2)*(gamma/Mj)
        self._sd_init(name, t0, dt, x, dx, omegaj, epsilonj, dr, di, B, zeta, left, right)

    def __str__(self):
        vel = "dzj/dt"
        if self.omegaj > 0:
            pos = "("+str(self.epsilonj)+"+"+str(self.omegaj)+"i)zj"
        else:
            pos = "("+str(self.epsilonj)+str(self.omegaj)+"i)Zj"
        if self.di > 0:
            coupl = "("+str(self.dr)+"+"+str(self.di)+"i)(zl+zr-2zj)"
        else:
            coupl = "("+str(self.dr)+str(self.di)+"i)(zl+zr-2zj)"
        if self.B < 0:
            unl = "+"+str(-self.B)+"|zj|^2zj"
        else:
            unl = str(-self.B)+"|zj|^2zj"
        if self.zeta.startswith("-"):
            force = self.zeta
        else:
            force = "+" + self.zeta
        return vel+"="+pos+"+"+coupl+force+unl

    def __eq__(self, other):
        return super().__eq__(other) and self.omegaj == other.omegaj and self.epsilonj == other.epsilonj and self.dr == other.dr and self.di == other.di and self.B == other.B and self.zeta == other.zeta and self.left == other.left and self.right == other.right