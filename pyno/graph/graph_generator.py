from types import FunctionType
from math import sin, cos, tan, exp, e, pi
from .math import sin_lut, SinLUT
from .graph import Graph

def create_function(function: str) -> FunctionType:
    # We first create the string that contains the function definition of f, which is just a function that returns the value of the equation evaluated at x
    fn = "def f(x):\n\treturn " + function
    ldict = {}
    # Execute the function and convert it into an actual Python function
    exec(fn, globals(), ldict)

    # TODO: This test might not fail on invaild functions, if the function does not fail on 0 (if it contains an if-statement that fails on not 0)
    # Test if function can be executed
    ldict['f'](0)

    # Return that function from the defined local scope
    return ldict['f']

def create_fa_function(function: str, lut: SinLUT = None) -> FunctionType:
    if function == "sin":
        def f(f, a, t):
            return a * sin(f * t)
        
        return f
    elif function == "sin_lut":
        def f(f, a, t):
            return sin_lut(f, a, t, lut)
        
        return f
    else:
        return _create_fa_function(function)

def _create_fa_function(function: str) -> FunctionType:
    # We first create the string that contains the function definition of f, which is just a function that returns the value of the equation evaluated at x
    fn = "def f(f, a, t):\n\treturn " + function
    ldict = {}
    # Execute the function and convert it into an actual Python function
    exec(fn, globals(), ldict)

    # Test if function can be executed
    ldict['f'](0, 0, 0)

    # Return that function from the defined local scope
    return ldict['f']

def generate_list(t0: float, dt: float, N: int, f: FunctionType) -> list[float]:
    res = []

    # Go through numbers from 0 to N and evaluate the inputed function at that point, based on t0 and dt, append it to the results list
    # TODO: This needs to use list comprehension
    for i in range(N):
        res.append(f(t0 + i * dt))
    
    return res

def graph_from_equation(equation: str, t0: float, dt: float, N: int) -> Graph:
    # Convert the string equation into an actual equation and generate the list based on that equation
    g = Graph(equation, t0 = t0, dt = dt, x = generate_list(t0, dt, N, create_function(equation)))
    
    # Calculate dx from graph
    g.calc_dx()
    return g
