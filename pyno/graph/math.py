import numpy
import math

class SinLUT:
    min_freq = None
    step_freq = None
    t0 = None
    dt = None
    
    lut = []
        
    def __init__(self, min_freq: float, max_freq: float, step_freq: float, t0: float, dt: float, N: int) -> None:
        self.min_freq = min_freq
        self.step_freq = step_freq
        self.t0 = t0
        self.dt = dt

        # Add 50 to N to make sure we do not run out of numbers, the solvers tend to overshoot a little in time
        self.lut = [[math.sin(f * (t0 + dt * ti)) for f in numpy.arange(min_freq, max_freq, step_freq)] for ti in range(N + 50)]

    def __getitem__(self, item):
        return self.lut[item]

def sin_lut(f: float, a: float, t: float, sin_lut: SinLUT):
    return sin_lut.lut[round((t - sin_lut.t0) / sin_lut.dt)][round((f - sin_lut.min_freq) / sin_lut.step_freq)] * a