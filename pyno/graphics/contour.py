import numpy as np
import math
import itertools
from ..entrainment import ArnoldTongues

def blur(windings: list[list[float]], type: str):
    avg_kern = np.array(([1,1,1],[1,1,1],[1,1,1]))*1/9
    gaus_kern = np.array(([1,2,1], [2,4,2], [1,2,1]))*1/16
    avg_corner_val = 9/4
    avg_side_val = 9/6
    gaus_corner_val = 16/9
    gaus_side_val = 4/3
    max_y = len(windings)
    max_x = len(windings[0])
    res = [[0 for _ in range(max_x)] for _ in range(max_y)]
    if type == "Gaussian":
        kern = gaus_kern
        corner_val = gaus_corner_val
        side_val = gaus_side_val
    else:
        kern = avg_kern
        corner_val = avg_corner_val
        side_val = avg_side_val
    for y in range(max_y):
        for x in range(max_x):
            if y == 0 and x == 0:
                res[y][x] = np.sum(np.array((kern[0][1:], kern[1][1:])) * np.array((windings[y+1][:2], windings[y][:2])))*corner_val
            elif y == max_y - 1 and x == 0:
                res[y][x] = np.sum(np.array((kern[1][1:], kern[2][1:])) * np.array((windings[y][:2], windings[y-1][:2])))*corner_val
            elif y == 0 and x == max_x - 1:
                res[y][x] = np.sum(np.array((kern[0][:2], kern[1][:2])) * np.array((windings[y+1][x-1:], windings[y][x-1:])))*corner_val
            elif y == max_y - 1 and x == max_x - 1:
                res[y][x] = np.sum(np.array((kern[1][:2], kern[2][:2])) * np.array((windings[y][x-1:], windings[y-1][x-1:])))*corner_val
            elif x == 0:
                res[y][x] = np.sum(np.array((kern[0][1:], kern[1][1:], kern[2][1:])) * np.array((windings[y+1][x:x+2], windings[y][x:x+2], windings[y-1][x:x+2])))*side_val
            elif x == max_x - 1:
                res[y][x] = np.sum(np.array((kern[0][:2], kern[1][:2], kern[2][:2])) * np.array((windings[y+1][x-1:x+1], windings[y][x-1:x+1], windings[y-1][x-1:x+1])))*side_val
            elif y == 0:
                res[y][x] = np.sum(np.array((kern[0], kern[1])) * np.array((windings[y+1][x-1:x+2], windings[y][x-1:x+2])))*side_val
            elif y == max_y - 1:
                res[y][x] = np.sum(np.array((kern[1], kern[2])) * np.array((windings[y][x-1:x+2], windings[y-1][x-1:x+2])))*side_val
            else:
                ar = np.array((windings[y+1][x-1:x+2], windings[y][x-1:x+2], windings[y-1][x-1:x+2]))
                res[y][x] = np.sum(kern*ar)

    return res


# def get_contour(level: float, at: ArnoldTongues):
#     points_list = []
#     x_list = np.arange(at.f_min, at.f_max, at.df)
#     poly_list = _get_polynomials(x_list, at.windings)
#     for i, p in enumerate(poly_list):
#         points = []
#         for point in (p - level).roots():
#             points.append(point)
#         points_list.append((points, at.a_min+i*at.da))

#     return _contour(points_list)


# def _get_polynomials(x_list: list[float], z_list: list[list[float]]) -> list[np.polynomial.polynomial.Polynomial]:
#     res = []
#     for z in z_list:
#         coeff = np.polynomial.polynomial.polyfit(x_list, list(map(lambda n : min(n,1.2), z)), 4)
#         poly = np.polynomial.polynomial.Polynomial(coeff)
#         res.append(poly)

#     return res


# def _contour(points: list[tuple[list[float], float]]) -> list[tuple[tuple[float, int], tuple[float, int]]]:
#     res = []
#     for ((yl, y), (yln, yn)) in itertools.pairwise(points):
#         for cur_p in yl:
#             min_dist = math.inf
#             min_point = None
#             for test_p in yln:
#                 dist = abs(cur_p - test_p)
#                 if dist < min_dist:
#                     min_dist = dist
#                     min_point = test_p

#             res.append(((cur_p, y), (min_point, yn)))

#     return res