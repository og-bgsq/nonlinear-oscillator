import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import math
from enum import Enum
from dataclasses import dataclass
from matplotlib.transforms import Bbox
from ..graph import Graph
from ..entrainment import ArnoldTongues
from .contour import blur

matplotlib.rcParams['figure.dpi'] = 300

T10_GRAPH_COLORS = [(0x1f/255.0, 0x77/255.0, 0xb4/255.0),
                    (0xff/255.0, 0x7f/255.0, 0x0e/255.0),
                    (0x2c/255.0, 0xa0/255.0, 0x2c/255.0),
                    (0x94/255.0, 0x67/255.0, 0xbd/255.0),
                    (0x8c/255.0, 0x56/255.0, 0x4b/255.0),
                    (0xe3/255.0, 0x77/255.0, 0xc2/255.0),
                    (0x7f/255.0, 0x7f/255.0, 0x7f/255.0),
                    (0xbc/255.0, 0xbd/255.0, 0x22/255.0),
                    (0x17/255.0, 0xbe/255.0, 0xcf/255.0),
                    (0xd6/255.0, 0x27/255.0, 0x28/255.0)]

class StyleFlags(Enum):
    AUTO_SCALE = 1,
    GRID = 2,
    LOG_LOG = 4,
    MINOR_TICKS = 8,
    EQUAL_ASPECT = 16

class PlotType(Enum):
    NORMAL = 1,
    STEM = 2,
    POINT = 3,
    STEP = 4,

@dataclass
class Style:
    annotations: list[tuple[float, float, str]]
    styles: list[StyleFlags]
    type: PlotType
    legends: list[str]
    text: list[tuple[float, float, str]]
    title: str
    xlabel: str
    ylabel: str

    def __post_init__(self):
        if self.annotations is None:
            self.annotations = []
        if self.styles is None:
            self.styles = []
        if self.type is None:
            self.type = PlotType.NORMAL
        if self.legends is None:
            self.legends = []
        if self.text is None:
            self.text = []
        if self.title is None:
            self.title = ""
        if self.xlabel is None:
            self.xlabel = ""
        if self.ylabel is None:
            self.ylabel = ""

TIME_PLOT_STYLE = Style([], [StyleFlags.GRID], PlotType.STEM, [], [], "", "Time", "Amplitude")
PHASE_SPACE_STYLE = Style([], [StyleFlags.GRID, StyleFlags.EQUAL_ASPECT], PlotType.POINT, [], [], "", "Amplitude", "Velocity")

phase_cols = []

curr_figures: list[tuple[plt.Figure, list[str]]] = []

def plot_time_phase(graphs: list[Graph], split: bool = True, legend: bool = False):
    if split:
        time_fig, phase_fig, time_axs, phase_axs = _time_phase_init_split(len(graphs))

        time_fig.set_size_inches(3.5, 3.5 * len(time_axs)/len(time_axs[0]))
        phase_fig.set_size_inches(3.5, 3.5 * len(phase_axs)/len(phase_axs[0]))

        max_len = 0
        for g in graphs:
            if len(g.x) > max_len:
                max_len = len(g.x)

        _calc_plot_color(len(graphs), max_len, 500)

        _set_time_phase_style_split(time_axs, phase_axs, time_fig, phase_fig)

        _plot_time_phase_graphs_split(time_axs, phase_axs, graphs)

        _post_time_phase_plot_split(time_axs, phase_axs, legend)

        abs_max = 0
        for phase_ax in phase_axs.flat:
            abs_max = max(-phase_ax.get_xlim()[0], phase_ax.get_xlim()[1], -phase_ax.get_ylim()[0], phase_ax.get_ylim()[1], abs_max)

        label_max = math.floor(abs_max)

        abs_max *= 1.2

        for phase_ax in phase_axs.flat:
            phase_ax.set_ylim(-abs_max, abs_max)
            phase_ax.set_xlim(-abs_max, abs_max)

            phase_ax.set(xticks=[-label_max, 0, label_max], yticks=[-label_max, 0, label_max])

        plt.show()

        curr_figures.append((time_fig, [""]))
        curr_figures.append((phase_fig, [""]))

    else:
        time_ax, phase_ax, fig = _time_phase_graph_init()

        max_len = 0
        for g in graphs:
            if len(g.x) > max_len:
                max_len = len(g.x)

        _calc_plot_color(len(graphs), max_len, 500)

        _set_time_phase_style(time_ax, phase_ax, [g.name for g in graphs])

        _plot_time_phase_graphs(time_ax, phase_ax, graphs)

        _post_time_phase_plot(time_ax, phase_ax, legend)

        yabs_max = max(-phase_ax.get_ylim()[0], phase_ax.get_ylim()[1]) * 1.1
        xabs_max = max(-phase_ax.get_xlim()[0], phase_ax.get_xlim()[1]) * 1.1
        phase_ax.set_ylim(-yabs_max, yabs_max)
        phase_ax.set_xlim(-xabs_max, xabs_max)

        plt.show()

        curr_figures.append((fig,["time", "phase"]))

def plot_arnold_tongues(at: ArnoldTongues, levels: list[float] = None):
    CAP = 1.2

    fig = plt.figure()

    axis = fig.subplots()

    axis.set_xlabel("ω/ω₀")
    axis.set_ylabel("Amplitude")

    def _level_gen(levels: list[float]):
        for level in levels:
            yield level - 0.01
            yield level + 0.01

    map = axis.imshow(list(reversed(at.windings)), cmap='gray', vmax=CAP, extent=[at.f_min / at.omega_0, at.f_max / at.omega_0, at.a_min, at.a_max])
    fig.colorbar(map)
    if levels:
        contours = list(_level_gen(levels))
        contours.sort()
        plt.contour([x for x in np.arange(at.f_min, at.f_max, at.df)], [y for y in np.arange(at.a_min, at.a_max, at.da)],blur(blur(blur(at.windings, ""), ""), ""), levels=contours, colors='red', algorithm='serial')

    axis.set_aspect((at.f_min - at.f_max) / (at.a_min - at.a_max) / at.omega_0)
    axis.set_box_aspect(1)

    plt.show()

    curr_figures.append((fig, [""]))

def plot_graphs(graphs: list[tuple[Graph, Style]], nrow: int, ncol: int):
    if False:
        with plt.xkcd():
            fig = plt.figure()
    else:
        fig = plt.figure()

    for i in range(len(graphs)):
        y = graphs[i][0].x
        t = [graphs[i][0].t0 + i * graphs[i][0].dt for i in range(len(y))]
        plot_graph(fig, i + 1, nrow, ncol, graphs[i][1], (t, y))

    curr_figures.append((fig, [""]))

    plt.show()

def plot_graph(fig: plt.Figure, index: int, nrow: int, ncol: int, style: Style, graph: tuple[list[float], list[float]]):
    x = graph[0]
    y = graph[1]

    ax = fig.add_subplot(nrow, ncol, index)

    add_annotations(ax, style.annotations)
    fig.legend(style.legends)
    add_text(ax, style.text)
    plt.title(style.title)
    plt.xlabel(style.xlabel)
    plt.ylabel(style.ylabel)

    match style.type:
        case PlotType.STEM:
            ax.stem(x, y)
        case PlotType.POINT:
            ax.scatter(x, y)
        case PlotType.STEP:
            ax.step(x, y)
        case _:
            ax.plot(x, y)

    update_styles(ax, style.styles)

def add_annotations(sub_plot: plt.Axes, annotations: list[tuple[float, float, str]]):
    for an in annotations:
        sub_plot.annotate(an[2], (an[0], an[1]))

def add_text(fig: plt.Figure, text: list[tuple[float, float, str]]):
    for t in text:
        plt.figtext(t[0], t[1], t[2])

def update_styles(ax: plt.Axes, styles: list[StyleFlags]):
    for f in styles:
        match f:
            case StyleFlags.AUTO_SCALE:
                ax.autoscale()
            case StyleFlags.GRID:
                ax.grid(True)
            case StyleFlags.LOG_LOG:
                ax.log_log()
            case StyleFlags.MINOR_TICKS:
                ax.minorticks_on()
            case StyleFlags.EQUAL_ASPECT:
                ax.set_aspect('equal')

def write_image(location: str, formats: list[str] | str = "all", dpi: float = 500, width: float = 3.5):
    if formats == "all":
        formats = ["png", "svg"]
    
    formats = list(formats)

    for i, (fig, plot_types) in enumerate(curr_figures):
        axes = fig.axes

        f = plt.figure()
        m = f.canvas.manager
        m.canvas.figure = fig
        fig.set_canvas(m.canvas)

        height = width * axes[0].get_gridspec().nrows / axes[0].get_gridspec().ncols

        fig.set_size_inches(width, height)

        for form in formats:
            loc = location
            
            if len(curr_figures) > 1:
                loc = loc + "_" + str(i)
            else:
                loc = loc
         
            fig.savefig(loc + "." + form, format=form, dpi=dpi, bbox_inches="tight")

            if len(plot_types) < 2:
                continue

            for ax, tp in zip(axes, plot_types):
                extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
                fig.savefig(loc + "-" + tp + "." + form, format=form, bbox_inches=extent.expanded(1.7, 1.6), dpi=dpi)

def _time_phase_graph_init():
    fig = plt.figure()
    fig.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)

    time_ax = fig.add_subplot(1, 2, 1)
    phase_ax = fig.add_subplot(1, 2, 2)

    time_ax.tick_params(axis="both", direction="in")
    phase_ax.tick_params(axis="both", direction="in")

    fig.tight_layout(pad=3.5)

    return (time_ax, phase_ax, fig)

def _time_phase_init_split(num: int):
    time_fig = plt.figure()
    phase_fig = plt.figure()
    time_fig.subplots_adjust(wspace=0, hspace=0, bottom=0.15, left=0.16)
    phase_fig.subplots_adjust(wspace=0, hspace=0, bottom=0.15, left=0.16)

    def _calc_x_y(num: int):
        guess = int(math.sqrt(num))

        while num % guess != 0:
            guess -= 1
        
        return (num//guess, guess)
    
    w, h = _calc_x_y(num)

    time_axs = time_fig.subplots(h, w, sharex=True, sharey=True)
    phase_axs = phase_fig.subplots(h, w, sharex=True, sharey=True)

    if not isinstance(time_axs, np.ndarray):
        time_axs = np.array([[time_axs]])
        phase_axs = np.array([[phase_axs]])
    elif not isinstance(time_axs[0], np.ndarray):
        time_axs = np.array([time_axs])
        phase_axs = np.array([phase_axs])

    for time, phase in zip(time_axs.flat, phase_axs.flat):
        # Place ticks inside graph
        time.tick_params(axis="both", direction="in")
        phase.tick_params(axis="both", direction="in")

    return (time_fig, phase_fig, time_axs, phase_axs)


def _plot_graph(time_ax: plt.Axes, phase_ax: plt.Axes, t: list[float], x: list[float], dx: list[float], name: str, color_index: int = 0):
    stem_container = time_ax.stem(t, x, label=name)
    marker, stem, base = stem_container
    marker.set_markersize(3)
    marker.set_marker("D")
    marker.set_markerfacecolor('none')
    marker.set_color(T10_GRAPH_COLORS[color_index])
    marker.set_alpha(0.7)
    stem.set_color(T10_GRAPH_COLORS[color_index])
    stem.set_alpha(0.1)
    
    _extend_plot_color(len(dx), color_index)

    path_collection = phase_ax.scatter(x[0:len(dx)], dx, s = 3, color=phase_cols[color_index][-len(dx):])

    return (stem_container, path_collection)

def _plot_time_phase_graphs(time_ax: plt.Axes, phase_ax: plt.Axes, graphs: list[Graph]):
    for i in range(len(graphs)):
        t = [graphs[i].t0 + j * graphs[i].dt for j in range(len(graphs[i].x))]
        _plot_graph(time_ax, phase_ax, t, graphs[i].x, graphs[i].dx, str(graphs[i]), i)

def _plot_time_phase_graphs_split(time_axs: list[list[plt.Axes]], phase_axs: list[list[plt.Axes]], graphs: list[Graph]):
    for i, (time_ax, phase_ax, graph) in enumerate(zip(time_axs.flat, phase_axs.flat, graphs)):
        t = [graph.t0 + j * graph.dt for j in range(len(graph.x))]
        _plot_graph(time_ax, phase_ax, t, graph.x, graph.dx, str(graph), i)

def _post_time_phase_plot(time, phase, legend: bool):
    if legend:
        time.legend()

def _post_time_phase_plot_split(time_axs: list[list[plt.Axes]], phase_axs: list[list[plt.Axes]], legend: bool):
    if legend:
        for time in time_axs.flat:
            time.legend()

def _set_time_phase_style(time: plt.Axes, phase: plt.Axes, labels: list[str]):
    time.set_xlabel("Time")
    time.set_ylabel("x")
    time.grid(True)
    time.set_box_aspect(1)
    
    phase.set_xlabel("x")
    phase.set_ylabel("ẋ")
    phase.grid(True)
    phase.set_box_aspect(1)
    #phase.set_aspect('equal', adjustable='box')
    #phase.set_box_aspect(phase.bbox.height/phase.bbox.width)

def _set_time_phase_style_split(time_axs: list[list[plt.Axes]], phase_axs: list[list[plt.Axes]], time_fig: plt.Figure, phase_fig: plt.Figure):
    time_fig.supxlabel("Time")
    time_fig.supylabel("x")
    phase_fig.supxlabel("x")
    phase_fig.supylabel("ẋ")

    for time, phase in zip(time_axs.flat, phase_axs.flat):
        time.grid(True)
        #time.set_box_aspect(1)
        
        phase.grid(True)
        #phase.set_box_aspect(1)


def _calc_plot_color(N: int, num_vals: int, cap: int = 50):
    # THIS ONLY SUPPORTS 10 DIFFERENT EQUATIONS TO BE SHOWN AT THE SAME TIME!
    for i in range(N):
        cols = []
        t10_col = T10_GRAPH_COLORS[i]

        num_cols = min(num_vals, cap)
        for j in range(num_cols):
            perc = j / num_cols * 0.8 + 0.2
            cols.append((perc*t10_col[0]+1.0*(1.0 - perc), perc*t10_col[1]+1.0*(1.0-perc), perc*t10_col[2]+1.0*(1.0-perc)))
            #cols.append((t10_col[0], t10_col[1], t10_col[2], perc))
        
        phase_cols.append(cols)
    
def _extend_plot_color(N: int, index: int):
    if len(phase_cols[index]) < N:
        phase_cols[index][:0] = [phase_cols[index][0]] * (N - len(phase_cols[index]))