import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import time
from enum import Enum
from types import FunctionType
from .window import _set_time_phase_style, _time_phase_graph_init, _calc_plot_color, _plot_graph, _post_time_phase_plot

T10_GRAPH_COLORS = [(0x1f/255.0, 0x77/255.0, 0xb4/255.0),
                    (0xff/255.0, 0x7f/255.0, 0x0e/255.0),
                    (0x2c/255.0, 0xa0/255.0, 0x2c/255.0),
                    (0xd6/255.0, 0x27/255.0, 0x28/255.0),
                    (0x94/255.0, 0x67/255.0, 0xbd/255.0),
                    (0x8c/255.0, 0x56/255.0, 0x4b/255.0),
                    (0xe3/255.0, 0x77/255.0, 0xc2/255.0),
                    (0x7f/255.0, 0x7f/255.0, 0x7f/255.0),
                    (0xbc/255.0, 0xbd/255.0, 0x22/255.0),
                    (0x17/255.0, 0xbe/255.0, 0xcf/255.0)]

class FuncType(Enum):
    TIME_DEP_WO_D = 1,
    TIME_DEP_W_D = 2,
    AMP_VEL_DEP = 3,
    DELTA_T_DEP = 4,

def plot_time_phase_realtime(functions: list[tuple[FunctionType, FuncType]], t0: float, dt: float, fps: int = 60, num_vals: int = 20, show_fps: bool = True):
    x = [t * dt + t0 for t in range(num_vals)]
    ys = []
    dys = []
    
    # Initialize y and dy array to have same amount of elements as number of functions
    ys = [[0 for _ in range(num_vals)] for _ in range(len(functions))]
    dys = [[0 for _ in range(num_vals)] for _ in range(len(functions))]

    # Create two plots, with time above phase
    time_ax, phase_ax, fig = _time_phase_graph_init()

    # Calculate the colormap colors
    _calc_plot_color(len(functions), num_vals)

    # Create all the plots used every frame
    plots = [_plot_graph(time_ax, phase_ax, x, y, dy, "", i) for i, (y, dy) in enumerate(zip(ys, dys))]
    
    # Update plot to look like standard plot
    _post_time_phase_plot(time_ax, phase_ax, [])

    # Remove x ticks from time axis, so we do not have to update them
    time_ax.tick_params("x", which="both", bottom=False, top=False, labelbottom=False)

    y_max_time = 0
    y_max_phase = 0
    x_max_phase = 0

    #t = t0
    frames = 0
    last_time = time.time()

    # Only run loop if the figure still exists
    def animate(i):
        nonlocal y_max_time, y_max_phase, x_max_phase, frames, last_time

        # Create a list of all artists that have updated this frame
        updates = []

        t = t0 + dt * i

        # Re-set the styling for the plots (this needs to be done every frame, as we clear the styling and it would only show up once on the first fram)
        _set_time_phase_style(time_ax, phase_ax, [])

        updated_bounds = False
        
        for (fun, fun_type), y, dy, ((markerline, stemlines, baseline), paths) in zip(functions, ys, dys, plots):
            # Get the new y and dy values from function
            # If the y and dy arrays are empty, then give None, so the get_y_dy_from_fun function can handle it
            new_y, new_dy = get_y_dy_from_fun(fun, fun_type, t, dt, y[-1] if len(y) > 0 else None, dy[-1] if len(dy) > 0 else None)

            # Append the new values and remove the oldest
            y.append(new_y)
            dy.append(new_dy)
            del y[0]
            del dy[0]

            # Update max values to be the maximal value of the previous bests and the new value
            if y_max_time < abs(new_y):
                y_max_time = abs(new_y)
                updated_bounds = True
            if x_max_phase < abs(new_y):
                x_max_phase = abs(new_y)
                updated_bounds = True
            if y_max_phase < abs(new_dy):
                y_max_phase = abs(new_dy)
                updated_bounds = True

            # Plot the time and phase data
            markerline.set_ydata(y)
            stemlines.set_paths([[[x_val, 0], [x_val, y_val]] for (x_val, y_val) in zip(x, y)])
            paths.set_offsets(list(zip(y, dy)))

            # Add updated artists to list
            updates.append(markerline)
            updates.append(stemlines)
            updates.append(paths)

        # Set bounds of plots based on size of data, but make sure it does not shrink with data, only expands
        if updated_bounds:
            y_max_time, x_max_phase, y_max_phase = set_bounds(time_ax, phase_ax, y_max_time, x_max_phase, y_max_phase)

        # Update x axis of time plot
        time_ax.set_xlim(x[0], x[-1])

        # Add time step to time and pause (draw) plots
        t += dt

        frames += 1
        if show_fps and time.time() - last_time > 1.0:
            # One second has elapsed since last FPS update
            # Calculate the FPS by dividing the number of frames by the time it took to display those frames
            print("FPS:", frames / (time.time() - last_time), end="\r")

            frames = 0
            last_time = time.time()
        
        return updates
    
    anim = FuncAnimation(fig, animate, 100000, interval=1/fps*1000, blit=True)

    plt.show()


def get_y_dy_from_fun(fun: FunctionType, fun_type: FuncType, t: float, dt: float, y: float, dy: float) -> tuple[float, float]:
    # Based on the function type, we need to retreave the y and dy data differently
    match fun_type:
        case FuncType.TIME_DEP_WO_D:
            # In a time dependant function, without delta, we need to first get the value, based on the time
            # Then we need to calculate the delta by standard rise over run method
            cy = fun(t)
            if not y:
                # If the y is None (this is probably the first frame), then we need to calculate what the y was for the previous frame, by subtracting dt from t
                y = fun(t-dt)

            return (cy, (cy - y) / dt)
        
        case FuncType.TIME_DEP_W_D:
            # If the function is time dependant and has delta values, then we just return these values
            return fun(t)

        case FuncType.AMP_VEL_DEP:
            # If the function is amplitude and velocity dependant, then we return the value of the function
            if not (y and dy):
                # If y or dy does not exist, then we just initialize them to 0
                y = dy = 0
            
            return fun(y, dy)

        case FuncType.DELTA_T_DEP:
            # If the function is delta t dependant, then we return the value of the function at dt
            return fun(dt)
        
def set_bounds(time, phase, y_max_time, x_max_phase, y_max_phase):
    # First we need to caluculate the bounds, that the data is within and check if this is greater than the previous max (then update if it is)
    # To find what the bounding box of the data is, we need to get the max value of higer bound or the negation of the lower part.
    # If the lower part of the data is on the same quadrant as the higer part, then it is never further away from the center, else we need to find out if it is
    # A value of 1.1 is chosen as padding, as the data otherwise would be right on the edge of the screen
    #y_max_time = max(max(-time.get_ylim()[0], time.get_ylim()[1]) * 1.1, y_max_time)
    #x_max_phase = max(max(-phase.get_xlim()[0], phase.get_xlim()[1]) * 1.1, x_max_phase)
    #y_max_phase = max(max(-phase.get_ylim()[0], phase.get_ylim()[1]) * 1.1, y_max_phase)

    # We then update the axis to this new data
    time.set_ylim(-y_max_time * 1.1, y_max_time * 1.1)
    
    # If the data is bounded by the x values, then we need to fit the screen based on the x values and calculate the max y values from this.
    # Else we calculate the max x values from the y values
    if x_max_phase * phase.bbox.height/phase.bbox.width > y_max_phase:
        phase.set_xlim(-x_max_phase * 1.1, x_max_phase * 1.1)
        phase.set_ylim(-x_max_phase * phase.bbox.height/phase.bbox.width * 1.1, x_max_phase * phase.bbox.height/phase.bbox.width * 1.1)
    else:
        phase.set_xlim(-y_max_phase * phase.bbox.width/phase.bbox.height * 1.1, y_max_phase * phase.bbox.width/phase.bbox.height * 1.1)
        phase.set_ylim(-y_max_phase * 1.1, y_max_phase * 1.1)

    # Return the new limits
    return (y_max_time, x_max_phase, y_max_phase)