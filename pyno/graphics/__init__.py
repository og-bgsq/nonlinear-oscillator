from .window import plot_graphs, Style, StyleFlags, PlotType, write_image
from .window import plot_time_phase, plot_arnold_tongues
from .realtime_window import plot_time_phase_realtime, FuncType
