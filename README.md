# PyNO - A Python Nonlinear Oscillator Solver
An implementation of the solution to a nonlinear system described in the paper "Synchronization of nonlinear oscillators"
