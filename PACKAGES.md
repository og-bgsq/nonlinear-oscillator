# Packages used in this project
This is a list of all packages needed to run this project. All of the packages should be able to be installed using pip, as of writing this document
* numpy
* matplotlib
* scipy
* pytest