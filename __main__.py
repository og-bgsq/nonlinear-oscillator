import sys

from pyno import run

def main():
    input = None
    if not sys.stdin.isatty():
        input = sys.stdin.read()

    run(sys.argv, input)

if __name__ == "__main__":
    main()